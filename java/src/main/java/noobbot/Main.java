package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import com.google.gson.Gson;
import com.t42.engine.ThrottlePredictor;
import com.t42.messages.server.CarPositions;
import com.t42.messages.server.CarVelocities;
import com.t42.messages.server.GameInit;
import com.t42.messages.server.YourCar;
import com.t42.value.CarPosition;
import com.t42.value.TrackPiece;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    List<TrackPiece> pieces;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        double throttle = 0.5;
        ThrottlePredictor sp = null;
        YourCar yourCar = null;
        double lastAngle = 0;
        //TODO: viestien käsittely
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                //READ msg only once...
                CarPositions positions = gson.fromJson(line, CarPositions.class);
                CarPosition myCarPosition = getMyCarPosition(positions, yourCar);
                double endingSpeedForPiece = 0.5;//  sp.getEndingSpeedForPiece(positions, yourCar, false);
                //emergency break
                double carAngle = Math.abs(myCarPosition.getAngle());
                if ( lastAngle < carAngle && carAngle > 18) {
                    endingSpeedForPiece = endingSpeedForPiece / (carAngle/5);
                }
//                if ( lastAngle < myCarPosition.getAngle() && carAngle > 45) {
//                    endingSpeedForPiece = endingSpeedForPiece / 8;
//                }
                lastAngle = carAngle;
                System.out.println("gameTick");
                System.out.println(positions.getGameTick());
                System.out.println("speed set to:");
                System.out.println(endingSpeedForPiece);
                System.out.println(" angle:");
                System.out.println(myCarPosition.getAngle());
                send(new Throttle(endingSpeedForPiece));
            }else if ( msgFromServer.msgType.equals("carVelocities")) {
                CarVelocities velocity = gson.fromJson(line, CarVelocities.class);
                System.out.println("gameTick");
                System.out.println(velocity.getGameTick());
                System.out.println(" Vector speed: ");
                double x = velocity.getData().get(0).getX();
                double y = velocity.getData().get(0).getY();
                System.out.println(Math.abs(x)+Math.abs(y));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                System.out.println("yourcar");
                yourCar = gson.fromJson(line, YourCar.class);
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                //READ msg only once...
                final GameInit gameInit = gson.fromJson(line, GameInit.class);
                pieces = gameInit.getData().getRace().getTrack().getPieces();
//                sp = new ThrottlePredictor(gameInit);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                throttle = 1;
                send(new Throttle( throttle ));
            } else if (msgFromServer.msgType.equals("spawn")) {
                System.out.println("Spawn!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                throttle = 0.5;
                send(new Throttle( throttle ) );
            } else {
                send(new Ping());
            }
        }
    }

    private CarPosition getMyCarPosition(CarPositions positions, YourCar yourCar) {
        for ( CarPosition p : positions.getData() ) {
            if ( p.getId().getColor().equals(yourCar.getData().getColor())) {
                return p;
            }
        }
        //TODO.. what now...
        return new CarPosition();
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}