/**
 * 
 */
package com.t42.engine;

import static org.boon.Boon.puts;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.t42.value.Lane;
import com.t42.value.RaceSession;
import com.t42.value.TrackPiece;
import com.t42.value.TrackPiecePosition;
import com.t42.value.TurboAvailableData;

/**
 * @author hannuf
 *
 */
public enum TurboActivator {
    INSTANCE;

    private int currentLap = 0;
    private int maxLaps = 0;
    private boolean turboAvailable = false;
    private int turboDurationTicks = 0;
	private double turboFactor = 0.0;
	private int longestStraightIndex = -1;
	private int secondLongestStraightIndex = -1;
	private double secondLongestStraightLength = 0.0;
	private int turboActivatedTick = 0;
	private boolean turboActive = false;
	private double longestStraightLength = 0.0;
	private Integer lastStraightStartIndex = -1;
	private int turboUsedInLapCount = 0;
	private int turboAvailableInLapCount =0;
	private Map<Integer, Double> straightLengthMap = new TreeMap<Integer, Double>();
	
    private TrackDataCruncher trackDataCruncher = TrackDataCruncher.getInstance();
	private LinkedHashMap<Integer, Double> straightLengthMapSortedByValue;

    public static TurboActivator getInstance(){
        return INSTANCE;
    }

    public void init(RaceSession raceSession) {
    	this.maxLaps = raceSession.getLaps();
    	List<TrackPiece> trackPieces = trackDataCruncher.getTrackPieces();
    	boolean straightStarted = false;
    	lastStraightStartIndex = -1;
    	for (TrackPiece trackPiece : trackPieces) {
			if (trackPiece.isStraight()) {
				Double straightLength;
				if (!straightStarted) {
					straightStarted = true;
					lastStraightStartIndex = trackPiece.getIndex();
					straightLength = trackPiece.getLength();
				} else {
					straightLength = straightLengthMap.get(lastStraightStartIndex) + trackPiece.getLength();
				}

				if (lastStraightStartIndex > 0 && straightLength > longestStraightLength) {
					longestStraightIndex = lastStraightStartIndex;
					longestStraightLength = straightLength;
				}
				straightLengthMap.put(lastStraightStartIndex, straightLength);
			} else {
				straightStarted = false;
			}
		}
    	if (straightLengthMap.containsKey(0) && straightLengthMap.containsKey(lastStraightStartIndex)) {
	    	Double startingStraight = straightLengthMap.get(0);
	    	Double lastStraightBeforeStart = straightLengthMap.get(lastStraightStartIndex);
	    	Double straightLength = startingStraight + lastStraightBeforeStart;
	    	straightLengthMap.put(lastStraightStartIndex, straightLength);
			if (straightLength > longestStraightLength) {
				longestStraightIndex = lastStraightStartIndex;
				longestStraightLength = straightLength;
			}	    	
    	}
    	straightLengthMapSortedByValue = sortByValueDescending(straightLengthMap);
    	for (Entry<Integer, Double> entry: straightLengthMapSortedByValue.entrySet()) {
			if (entry.getKey() == longestStraightIndex) {
				continue;
			} else if (entry.getKey() > longestStraightIndex) {
				secondLongestStraightIndex = entry.getKey();
				secondLongestStraightLength = entry.getValue();
				break;
			}
		}
    	puts("straightLengthMap = " +straightLengthMapSortedByValue);
    	puts("Longest straight index = " + longestStraightIndex + ", longest straight length = " + longestStraightLength);
    	puts("Second Longest straight index = " + secondLongestStraightIndex + ", second longest straight length = " + secondLongestStraightLength);
    }

    private static LinkedHashMap<Integer, Double> sortByValueDescending(Map<Integer, Double> map) {
        List<Entry<Integer, Double>> list = new LinkedList<Entry<Integer, Double>>(map.entrySet());
        Collections.sort(list, new Comparator<Entry<Integer, Double>>() {
             public int compare(Entry<Integer, Double> o1, Entry<Integer, Double> o2) {
            	 // sort by length desc
            	 int valueComparison = o2.getValue().compareTo(o1.getValue());
            	 // sort by piece id asc
            	 if (valueComparison == 0) {
            		 return o1.getKey().compareTo(o2.getKey());
            	 } else {
            		 return valueComparison;
            	 }
             }
        });

        LinkedHashMap<Integer, Double> result = new LinkedHashMap<Integer, Double>();
       for (Iterator<Entry<Integer, Double>> it = list.iterator(); it.hasNext();) {
           Map.Entry<Integer, Double> entry = it.next();
           result.put(entry.getKey(), entry.getValue());
       }
       return result;
   }     
	public boolean isTurboActive() {
		return turboActive;
	}

	public double getTurboFactor() {
		if (turboActive) {
			return turboFactor;
		} else {
			return 1.0;
		}
	}
	public int getLongestStraightIndex() {
		return longestStraightIndex;
	}

	public void makeTurboAvailable(TurboAvailableData turboAvailableData) {
		this.turboAvailable = true;
		this.turboDurationTicks = turboAvailableData.getTurboDurationTicks();
		this.turboFactor = turboAvailableData.getTurboFactor();
		this.turboAvailableInLapCount++;
		puts("Turbo available for factor " + turboFactor + " and  turboDurationTicks = " + turboDurationTicks);
	}

	public boolean useTurbo(int tick) {
		TrackPiecePosition piecePosition = trackDataCruncher.getOurCarPosition( tick ).getPiecePosition();
        int trackPiecePosition = piecePosition.getPieceIndex();
        if (!turboActive && turboAvailable && longestStraightIndex == trackPiecePosition && isInTheBeginningOfTrackPiece(piecePosition)) {
			turboAvailable = false;
			turboActive = true;
			turboActivatedTick = tick;
			return true;
		} else if (!turboActive && turboAvailable && trackPiecePosition > longestStraightIndex) {
			if (trackPiecePosition == secondLongestStraightIndex && isInTheBeginningOfTrackPiece(piecePosition)) {
				turboAvailable = false;
				turboActive = true;
				turboActivatedTick = tick;
				return true;				
			}
		}
		
		if (!turboActive) {
			if (currentLap == maxLaps && turboAvailable && lastStraightStartIndex <= trackPiecePosition ) {
				turboAvailable = false;
				turboActive = true;
				turboActivatedTick = tick;
				return true;				
			}
		}
		
		return false;

	}

	private boolean isInTheBeginningOfTrackPiece(TrackPiecePosition piecePosition) {
		TrackPiece trackPiece = trackDataCruncher.getTrackPiece(piecePosition.getPieceIndex());
        Lane lane = trackDataCruncher.getTrack().getLane(piecePosition.getLane().getEndLaneIndex());
        if (trackPiece.getDistanceLeft(piecePosition.getInPieceDistance(), lane, trackDataCruncher.getTrack().getTrackDirection()) <= piecePosition.getInPieceDistance() / 4) {
        	return true;
        }
        return false;
	}

	public int getMaxLaps() {
		return maxLaps;
	}

	public void setTurboActive(boolean turboActive) {
		this.turboActive = turboActive;
		if (turboActive) {
			turboUsedInLapCount++;
		}
	}
	
	public void setCurrentLap(int lap) {
		this.currentLap = lap;
		this.turboUsedInLapCount = 0;
		this.turboAvailableInLapCount = 0;
	}
}
