package com.t42.engine;

import com.t42.value.*;

import java.util.*;

import static org.boon.Boon.puts;


/**
 *
 * crunches some track data
 *
 * todo:
 *
 *
 */
public enum TrackDataCruncher {

    INSTANCE;

    private Track track;
    private CarId ourCar;

    private boolean DEBUG = false;

    // lets not store state here
    //private double currentSpeed; // use getSpeed()
    //private Lane currentLane; // use getOwnLane or getCompetitorLane
    //private double currentPieceDistanceLeft; // use getTrackPieceDistanceLeft

    private NavigableMap<Integer,CarPosition> ourCarPositionHistory = new TreeMap<>();
    private Map<Integer,Double> ourCarSpeedHistoryPerTick = new HashMap<>();

    private List<CarId> competitorCars = new ArrayList<>();

    private Map<CarId,NavigableMap<Integer,CarPosition>> competitorCarPositionsHistory = new HashMap<>();
    private Map<CarId,Map<Integer,Double>> competitorCarsSpeedHistoryPerTick = new HashMap<>();


    public static TrackDataCruncher getInstance(){
        return INSTANCE;
    }

    /**
     *
     * calculate shortest route around track
     *  --> preferred lane for each piece
     * calculate target / max speeds for each (end of) piece
     *
     *
     * @param track
     * @return
     */
    public void init( Track track, CarId ourCar, List<Car> cars ){

        this.track = track;
        this.ourCar = ourCar;

        for( Car car : cars ){
            if( !car.getId().equals( ourCar ) ){
                competitorCars.add( car.getId() );
            }
        }

        // calculate track direction
        track.setTrackDirection( track.getTrackDirection() );
        track.setTrackPieces( createTrackPieceMap( track.getPieces() ) );
        calculatePreferredLanes( track );

        //puts( "length: ", atIndex( track.getPieces(), "length" ) );
        //puts( "switchPiece: ", atIndex( track.getPieces(), "switchPiece" ) );
        //puts( "radius: ", atIndex( track.getPieces(), "radius" ) );
        //puts( "angles: ", atIndex( track.getPieces(), "angle" ) );

        if( DEBUG ) puts( "TrackName: " + track.getName() );
        if( DEBUG ) puts( "TrackDirection: " + track.getTrackDirection() );
        if( DEBUG ) puts( "TrackPieces amount: " + track.getTrackPiecesAmount() );
        if( DEBUG ) puts( "Lanes amount: " + track.getLaneAmount() );
        if( DEBUG ) puts( "Competitors: " + competitorCars );

    }

    /**
     *
     * store car positions and calculate car speeds
     *
     */
    public void updateCarPositions( int tick, List<CarPosition> carPositions ){
        //puts( "updateCarPositions" + carPositions );

        for( CarPosition carPosition : carPositions ){
            if( carPosition.getId().equals( ourCar ) ){
                ourCarPositionHistory.put( tick, carPosition );
            }else{
                NavigableMap<Integer,CarPosition> competitorCarPositionHistory = competitorCarPositionsHistory.get( carPosition.getId() );
                if( competitorCarPositionHistory == null ) {
                    competitorCarPositionHistory = new TreeMap<Integer, CarPosition>();
                    competitorCarPositionsHistory.put( carPosition.getId(), competitorCarPositionHistory );
                }
                competitorCarPositionHistory.put( tick, carPosition );
            }
        }
        //currentSpeed = calculateOurCurrentSpeed(tick);
        calculateOurCurrentSpeed( tick );

        //TODO: do we need this? removed for now to speed up processing time
//        calculateCompetitorsCurrentSpeed( tick );

        //currentPieceDistanceLeft = getOurCarTrackPieceDistanceLeft(tick);
        //currentLane = getOurLane( tick );

        //puts( "[" + tick + "/" + getOurCarTrackPositionIndexOnTick( tick ) + "] updateCarPositions(), our current speed: " + currentSpeed );
    }


    public CarPosition getOurCarPosition(int tick){
        return ourCarPositionHistory.get(new Integer(tick));
    }

    public Lane getOurLane(int tick){
        return track.getLane(ourCarPositionHistory.get(new Integer(tick)).getPiecePosition().getLane().getStartLaneIndex());
    }

    public Lane getCompetitorLane(int tick, CarId carId){
        return track.getLane(competitorCarPositionsHistory.get(carId).get(tick).getPiecePosition().getLane().getStartLaneIndex());
    }

    /**
     * returns competitor cars on given trackPiece
     *
     * @param tick
     * @param trackPieceIndex
     * @return
     */
    public List<CarId> getCompetitorCarsOnTrackPiece(int tick, int trackPieceIndex){
        List<CarId> carsOnTrackPiece = new ArrayList<>();
        for( CarId carId : getCompetitorCars() ){
            if( getCompetitorCarPosition( tick, carId ).getPiecePosition().getPieceIndex() == trackPieceIndex ){
                carsOnTrackPiece.add( carId );
            }
        }
        return carsOnTrackPiece;
    }

    public int getTrackPiecesAmount() {
        return track.getTrackPiecesAmount();
    }

    public List<TrackPiece> getTrackPieces() {
        return new ArrayList<TrackPiece>( track.getTrackPieces().values() );
    }

    public TrackPiece getTrackPiece(int index) {
        return track.getTrackPiece(index);
    }

    public Track getTrack() {
        return track;
    }

    public CarId getOurCar() {
        return ourCar;
    }

    /**
     *
     * returns trackpiece ahead from given position
     *
     * @param currentTrackPiece
     * @param piecesAhead
     * @return
     */
    public TrackPiece getTrackPieceAhead( TrackPiece currentTrackPiece, int piecesAhead ) {
        int trackPiecesAmount = getTrackPiecesAmount();
        int pieceIndex = currentTrackPiece.getIndex() + piecesAhead;
        if ( trackPiecesAmount <= pieceIndex  ) {
            pieceIndex = pieceIndex - trackPiecesAmount;
        }
//        puts("current index: " + current.getIndex());
//        puts("return pieceindex: " + pieceIndex);
        return getTrackPiece(pieceIndex);
    }

    /**
     * returns trackpiece behind from given position
     *
     * @param currentTrackPiece
     * @param piecesBehind
     * @return
     */
    public TrackPiece getTrackPieceBehind( TrackPiece currentTrackPiece, int piecesBehind ) {
        int trackPiecesAmount = getTrackPiecesAmount();
        int pieceIndex = currentTrackPiece.getIndex() - piecesBehind;
        if ( pieceIndex < 0 ) {
            pieceIndex = trackPiecesAmount + pieceIndex;
        }
        return getTrackPiece(pieceIndex);
    }


    public TrackPiece getNextCurve( int startIndex ){
        int trackPieceIndex = track.getNextCurveIndex( startIndex );
        if( trackPieceIndex == -1 ){
            trackPieceIndex = track.getNextCurveIndex( 0 );
        }
        return getTrackPiece(trackPieceIndex);
    }

    public TrackPiece getNextSwitch( int startIndex ){
        int trackPieceIndex = track.getNextSwitchIndex(  startIndex);
        if( trackPieceIndex == -1 ){
            trackPieceIndex = track.getNextSwitchIndex(0);
        }
        return getTrackPiece(trackPieceIndex);
    }

    /**
     * returns our speed on tick
     *
     * @param tick
     * @return
     */
    public double getOurSpeed( int tick ){
        double speed = 0.0;
        if( ourCarSpeedHistoryPerTick.containsKey( tick ) ){
            speed = ourCarSpeedHistoryPerTick.get(tick);
        }
        return speed;
    }

    /**
     * returns competitors speed on tick
     *
     * @param tick
     * @param carId
     * @return
     */
    public double getCompetitorSpeed( int tick, CarId carId ){
        return competitorCarsSpeedHistoryPerTick.get( carId ).get( tick );
    }

    /**
     * returns the list of competitors
     * @return
     */
    public List<CarId> getCompetitorCars(){
        return competitorCars;
    }

    /**
     * returns competitors CarPosition on tick
     *
     * @param tick
     * @param carId
     * @return
     */
    public CarPosition getCompetitorCarPosition( int tick, CarId carId ){
        return competitorCarPositionsHistory.get( carId ).get( tick );
    }


    /**
     * get next trackpiece if it's type is totally different than current piece type
     * May return null!
     * @param tick
     * @return
     */
    public DistanceToNextKeyPiece getDistanceToNextKeyPiece(int tick) {
        CarPosition carPosition = getOurCarPosition(tick);
        double distance = getOurCarTrackPieceDistanceLeft(tick);
        TrackPiece currentTrackPiece = track.getTrackPiece(carPosition.getPiecePosition().getPieceIndex());
        Lane ourLane = getOurLane(tick);
        DistanceToNextKeyPiece mainKeyPiece = findNextKeyPiece(distance, currentTrackPiece, ourLane);
        DistanceToNextKeyPiece keyPiece2 = findNextKeyPiece(mainKeyPiece.getToPiece().getLengthForStraightOrCurve(ourLane, track.getTrackDirection()), mainKeyPiece.getToPiece(), ourLane);
        DistanceToNextKeyPiece keyPiece3 = findNextKeyPiece(keyPiece2.getToPiece().getLengthForStraightOrCurve(ourLane, track.getTrackDirection()), keyPiece2.getToPiece(), ourLane);
        mainKeyPiece.setKeyPiece2(keyPiece2);
        mainKeyPiece.setKeyPiece3(keyPiece3);
        return mainKeyPiece;
    }

    private DistanceToNextKeyPiece findNextKeyPiece(double distance, TrackPiece currentTrackPiece, Lane ourLane) {
        DistanceToNextKeyPiece distanceToNextKeyPiece = new DistanceToNextKeyPiece();
        distanceToNextKeyPiece.setFromPiece(currentTrackPiece);
        int i = 1;
        boolean keyPieceFound = false;
        TrackPiece previousKeyCandidate = null;
        while ( !keyPieceFound && i <= track.getTrackPiecesAmount() ) {
            TrackPiece keyCandidate = getTrackPieceAhead(currentTrackPiece, i);
            if ( previousKeyCandidate != null ) {
                distance = distance + keyCandidate.getLengthForStraightOrCurve(ourLane, track.getTrackDirection());
            }
            boolean keyCandidateIsAnotherCurve = isKeyCandidateAnotherCurve(currentTrackPiece, keyCandidate);
            keyPieceFound = (currentTrackPiece.isCurve() && keyCandidate.isStraight()) || (currentTrackPiece.isStraight() && keyCandidate.isCurve() || keyCandidateIsAnotherCurve);
            if (keyPieceFound) {
                distanceToNextKeyPiece.setToPiece(keyCandidate);
                distanceToNextKeyPiece.setDistance(distance);
            } else {
                i++;
                previousKeyCandidate = keyCandidate;
            }
        }
        return distanceToNextKeyPiece;
    }

    private boolean isKeyCandidateAnotherCurve(TrackPiece current, TrackPiece candidate) {
        return current.isCurve() && candidate.isCurve() && (current.getAngle() != candidate.getAngle() || current.getRadius() != candidate.getRadius());
    }


    /**
     *
     * returns our cars distance left on trackpiece
     *
     * @param tick
     * @return
     */
    public double getOurCarTrackPieceDistanceLeft( int tick ){
        double pieceDistanceLeft = 0.0;
        CarPosition ourCarPosition = getOurCarPosition( tick );
        TrackPiecePosition ourCarTrackPiecePosition = ourCarPosition.getPiecePosition();
        Lane ourCarLane = getOurLane(tick);
        pieceDistanceLeft = getTrackPieceDistanceLeft( ourCarTrackPiecePosition, ourCarLane );
        return pieceDistanceLeft;
    }

    /**
     *
     * returns competitor cars distance left on trackpiece
     *
     * @param tick
     * @param carId
     * @return
     */
    public double getCompetitorCarTrackPieceDistanceLeft( int tick, CarId carId ){
        double pieceDistanceLeft = 0.0;
        CarPosition ourCarPosition = getOurCarPosition( tick );
        CarPosition carPosition = getCompetitorCarPosition( tick, carId );

        TrackPiecePosition ourCarTrackPiecePosition = ourCarPosition.getPiecePosition();
        Lane ourCarLane = getOurLane(tick);
        pieceDistanceLeft = getTrackPieceDistanceLeft( ourCarTrackPiecePosition, ourCarLane );
        return pieceDistanceLeft;
    }

    /**
     *
     * returns distance left on trackpiece
     *
     * @param trackPiecePosition
     * @param carLane
     * @return
     */
    public double getTrackPieceDistanceLeft( TrackPiecePosition trackPiecePosition, Lane carLane ) {
        TrackPiece trackPiece = track.getTrackPiece( trackPiecePosition.getPieceIndex() );
        double inPieceDistance = trackPiecePosition.getInPieceDistance();
        double pieceDistanceLeft = trackPiece.getDistanceLeft( inPieceDistance, carLane, track.getTrackDirection() );
        return pieceDistanceLeft;
    }

    /**
     *
     * calculates ours SpeedPerTick, saves maxspeed and maxangle to trackpiece
     *
     * @param tick
     * @return
     */
    private double calculateOurCurrentSpeed(int tick) {
        //puts(  "calculateOurCurrentSpeed" );

        double currentSpeedPerTick = 0.0;

        if( tick > 0 ){
            TrackPiecePosition currentCarTrackPosition = ourCarPositionHistory.get( new Integer( tick ) ).getPiecePosition();
            TrackPiecePosition previousCarTrackPosition = ourCarPositionHistory.lowerEntry( tick ).getValue().getPiecePosition();
            CarPosition currentCarPosition = ourCarPositionHistory.get( new Integer( tick ) );

            double currentAngle = currentCarPosition.getAngle();
            // dirty fix to handle trackchanges in curves
            if( currentCarTrackPosition.getPieceIndex() != previousCarTrackPosition.getPieceIndex() ){
                currentSpeedPerTick = ourCarSpeedHistoryPerTick.get( new Integer( tick-1 ) );
            }else{
                currentSpeedPerTick = calculateSpeed( currentCarTrackPosition, previousCarTrackPosition );
            }
            int trackPieceIndex = currentCarTrackPosition.getPieceIndex();

            if( track.getTrackPiece( trackPieceIndex ).getMaxSpeed() < currentSpeedPerTick ){
                track.getTrackPiece( trackPieceIndex ).setMaxSpeed( currentSpeedPerTick );
                if( DEBUG ) puts( "T42, new maxSpeed: " + currentSpeedPerTick + ", angle: " + currentAngle + ", trackpiece: " + trackPieceIndex );
            }else{
                if( DEBUG ) puts("T42, speed: " + currentSpeedPerTick + ", angle: " + currentAngle + ", trackpiece: " + trackPieceIndex);
            }

            if( track.getTrackPiece( trackPieceIndex ).getMaxAngle() < Math.abs( currentAngle ) ){
                track.getTrackPiece( trackPieceIndex ).setMaxAngle(Math.abs(currentAngle));
                if( DEBUG ) puts( "T42, new maxAngle: " + currentAngle + ", trackpiece: " + trackPieceIndex );
            }
            ourCarSpeedHistoryPerTick.put( new Integer( tick ), new Double( currentSpeedPerTick ) );
        }
        return currentSpeedPerTick;
    }

    /**
     *
     * calculates SpeedPerTick for all competitors
     *
     * @param tick
     * @return
     */
    private double calculateCompetitorsCurrentSpeed( int tick ) {

        double currentSpeedPerTick = 0.0;

        if( tick > 0 ){
            for( Map.Entry<CarId,NavigableMap<Integer,CarPosition>> carPositionsHistory : competitorCarPositionsHistory.entrySet() ){
                CarId competitorCar = carPositionsHistory.getKey();
                NavigableMap<Integer,CarPosition> carPositionHistory = carPositionsHistory.getValue();

                TrackPiecePosition currentCarTrackPosition = carPositionHistory.get( new Integer( tick ) ).getPiecePosition();
                TrackPiecePosition previousCarTrackPosition = carPositionHistory.lowerEntry( tick ).getValue().getPiecePosition();
                currentSpeedPerTick = calculateSpeed( currentCarTrackPosition, previousCarTrackPosition );
                CarPosition currentCarPosition = competitorCarPositionsHistory.get( competitorCar ).get(new Integer(tick));

                double currentAngle = currentCarPosition.getAngle();
                Map<Integer,Double> competitorCarSpeedHistoryPerTick = competitorCarsSpeedHistoryPerTick.get( competitorCar );
                if( competitorCarSpeedHistoryPerTick == null ) {
                    competitorCarSpeedHistoryPerTick = new HashMap<Integer, Double>();
                    competitorCarSpeedHistoryPerTick.put( new Integer( tick ), new Double( currentSpeedPerTick ) );
                }

                int trackPieceIndex = currentCarTrackPosition.getPieceIndex();
                if( track.getTrackPiece( trackPieceIndex ).getMaxSpeed() < currentSpeedPerTick ){
                    track.getTrackPiece( trackPieceIndex ).setMaxSpeed( currentSpeedPerTick );
                    if( DEBUG ) puts( competitorCar.getName() + ", new maxSpeed: " + currentSpeedPerTick + ", angle: " + currentAngle + ", trackpiece: " + trackPieceIndex );
                }else{
                    if( DEBUG ) puts( competitorCar.getName() + ", speed: " + currentSpeedPerTick + ", angle: " + currentAngle + ", trackpiece: " + trackPieceIndex );
                }

                if( track.getTrackPiece( trackPieceIndex ).getMaxAngle() < Math.abs( currentAngle ) ){
                    track.getTrackPiece( trackPieceIndex ).setMaxAngle( Math.abs( currentAngle ) );
                    if( DEBUG ) puts( competitorCar.getName() + ", new maxAngle: " + currentAngle + ", trackpiece: " + trackPieceIndex );
                }
            }
        }
        return currentSpeedPerTick;
    }

    /**
     * calculates speed between two trackpositions
     *
     * @param currentCarPosition
     * @param previousCarPosition
     * @return
     */
    private double calculateSpeed( TrackPiecePosition currentCarPosition, TrackPiecePosition previousCarPosition ) {
        double currentSpeedPerTick;
        double currentInPieceDistance = currentCarPosition.getInPieceDistance();
        double lastInPieceDistance = previousCarPosition.getInPieceDistance();

        int currentPieceIndex = currentCarPosition.getPieceIndex();
        int lastPieceIndex = previousCarPosition.getPieceIndex();

        // quick switchpiece bugfix to get rid of negative speeds
        int currentCarLane = 0; //currentCarPosition.getLane().getEndLaneIndex();
        int lastCarLane = 0; //previousCarPosition.getLane().getEndLaneIndex();

        //puts( "SPEED: currentCarLane: " + currentCarLane );
        //puts( "SPEED: lastCarLane: " + lastCarLane );
        //puts( "SPEED: currentPieceIndex: " + currentPieceIndex );
        //puts( "SPEED: lastPieceIndex: " + lastPieceIndex );
        //puts( "SPEED: currentInPieceDistance: " + currentInPieceDistance );
        //puts( "SPEED: lastInPieceDistance: " + lastInPieceDistance );
        if( currentPieceIndex == lastPieceIndex ){
            currentSpeedPerTick = currentInPieceDistance-lastInPieceDistance;
        } else {
            double lastTrackPieceLength = track.getTrackPieces().get(lastPieceIndex).getLength();
            //puts( "SPEED: lastTrackPieceLength: " + lastTrackPieceLength );
            if( lastTrackPieceLength == 0.0 ){ // means curve

                int lastTrackPieceLaneIndex = 0; //currentCarPosition.getLane().getEndLaneIndex();
                Lane lastTrackPieceLane = track.getLane( lastTrackPieceLaneIndex );
                lastTrackPieceLength = track.getTrackPieces().get( lastPieceIndex ).getCurveLength( lastTrackPieceLane, track.getTrackDirection() );
                //puts( "SPEED: lastTrackPieceLength: " + lastTrackPieceLength + ", curve corrected");
            }

            double lastTrackDistanceTravelled = lastTrackPieceLength-lastInPieceDistance;
            //puts( "SPEED: lastTrackDistanceTravelled: " + lastTrackDistanceTravelled );
            currentSpeedPerTick = lastTrackDistanceTravelled+currentInPieceDistance;
        }
        return currentSpeedPerTick;
    }

    /**
     *
     * calculates initial preferred lanes to drive based on TrackDirection, upcoming curve and switch pieces
     *
     * @param track
     */
    private void calculatePreferredLanes( Track track ) {

        if( DEBUG ) puts( " -----------------" );
        if( DEBUG ) puts( "calculatePreferredLanes: " );

        NavigableMap<Integer, TrackPiece> switchTrackPieces = getSwitchTrackPieceNavigableMap( track );
        NavigableMap<Integer, TrackPiece> curveTrackPieces = getCurveTrackPieceNavigableMap( track );

        // combine maps
        NavigableMap<Integer,TrackPiece> switchAndCurveTrackPieces = new TreeMap<>();
        switchAndCurveTrackPieces.putAll( switchTrackPieces );
        switchAndCurveTrackPieces.putAll( curveTrackPieces );

        // loop through switch pieces
        Map.Entry<Integer, TrackPiece> previousTrackPieceEntry = null;

        for( Map.Entry<Integer, TrackPiece> trackPieceEntry : switchTrackPieces.entrySet() ) {

            // skip first entry
            if( previousTrackPieceEntry != null ){
                boolean includePrevious = previousTrackPieceEntry.getValue().isSwitchPiece() ? true : false;
                boolean includeCurrent = trackPieceEntry.getValue().isSwitchPiece() ? true : false;
                Map<Integer,TrackPiece> curvePiecesBetweenSwitchPieces = switchAndCurveTrackPieces.subMap(
                        previousTrackPieceEntry.getKey(), includePrevious, trackPieceEntry.getKey(), includeCurrent );
                Direction preferredSwitchDirection = getPreferredLaneSwitchDirection( curvePiecesBetweenSwitchPieces );
                switchAndCurveTrackPieces.get( previousTrackPieceEntry.getKey() ).setPreferredSwitchDirection( preferredSwitchDirection );
            }

            previousTrackPieceEntry = trackPieceEntry;
        }

        // calculate last switch entry
        boolean includePrevious = previousTrackPieceEntry.getValue().isSwitchPiece() ? true : false;
        boolean includeLast = switchTrackPieces.lastEntry().getValue().isSwitchPiece() ? true : false;
        Map.Entry<Integer, TrackPiece> lastTrackPieceEntry = switchAndCurveTrackPieces.lastEntry();

        Map<Integer,TrackPiece> curvePiecesForTrackEnd = switchAndCurveTrackPieces.subMap(
                previousTrackPieceEntry.getKey(), includePrevious, lastTrackPieceEntry.getKey(), includeLast );

        // then same for the beginning of track
        Map.Entry<Integer, TrackPiece> firstTrackPieceEntry = switchAndCurveTrackPieces.firstEntry();
        boolean includeFirst = firstTrackPieceEntry.getValue().isSwitchPiece() ? true : false;
        includeLast = switchTrackPieces.firstEntry().getValue().isSwitchPiece() ? true : false;

        Map<Integer,TrackPiece> curvePiecesForTrackStart = switchAndCurveTrackPieces.subMap(
                firstTrackPieceEntry.getKey(), includeFirst, switchTrackPieces.firstEntry().getKey(), includeLast );

        // combine maps
        NavigableMap<Integer,TrackPiece> lastCurves = new TreeMap<>();
        lastCurves.putAll( curvePiecesForTrackEnd );
        lastCurves.putAll( curvePiecesForTrackStart );

        Direction preferredSwitchDirection = getPreferredLaneSwitchDirection( lastCurves );
        switchAndCurveTrackPieces.get( previousTrackPieceEntry.getKey() ).setPreferredSwitchDirection( preferredSwitchDirection );

        // DONE!


        // just print out all
        for( Map.Entry<Integer, TrackPiece> trackPieceEntry : switchAndCurveTrackPieces.entrySet() ){
            if( DEBUG ) puts( "trackpieces: " + trackPieceEntry.getValue().getIndex() + ", " + trackPieceEntry.getValue().getTrackPieceType() +
                    ", switchDirection: " + trackPieceEntry.getValue().getPreferredSwitchDirection() );
        }

    }

    /**
     * returns preferred lane switch direction based on amounts of turns for different direction
     * @param curvePiecesBetweenSwitchPieces
     * @return
     */
    private Direction getPreferredLaneSwitchDirection( Map<Integer, TrackPiece> curvePiecesBetweenSwitchPieces ) {
        int left = 0;
        int right = 0;

        for( Map.Entry<Integer, TrackPiece> trackPieceEntry : curvePiecesBetweenSwitchPieces.entrySet() ){
            if( trackPieceEntry.getValue().getCurveDirection().equals( Direction.Left) ){
                left++;
            }else{
                right++;
            }
        }
        if( left == right ){
            return Direction.Straight;
        }
        return left > right ? Direction.Left : Direction.Right;
    }

    private NavigableMap<Integer, TrackPiece> getCurveTrackPieceNavigableMap(Track track) {
        int trackPieceIndex = 0;
        NavigableMap<Integer,TrackPiece> curveTrackPieces = new TreeMap<>();
        while( trackPieceIndex != -1 ){
            trackPieceIndex++; // start from next
            trackPieceIndex = track.getNextCurveIndex( trackPieceIndex );

            if( trackPieceIndex != -1 ){
                TrackPiece trackPiece = track.getTrackPiece( trackPieceIndex );
                if( DEBUG ) puts( "next curve trackPieceIndex: " + trackPieceIndex + ", " + trackPiece.getCurveDirection() +
                        ", angle: " + trackPiece.getAngle() );
                curveTrackPieces.put(trackPieceIndex, trackPiece);
            }
        }
        return curveTrackPieces;
    }

    /**
     * NavigableMap of switch trackpieces
     * @param track
     * @return
     */
    private NavigableMap<Integer, TrackPiece> getSwitchTrackPieceNavigableMap(Track track) {
        NavigableMap<Integer,TrackPiece> switchTrackPieces = new TreeMap<>();
        int trackPieceIndex = 0;
        while( trackPieceIndex != -1 ){
            trackPieceIndex++; // start from next
            trackPieceIndex = track.getNextSwitchIndex( trackPieceIndex );

            if( trackPieceIndex != -1 ){
                TrackPiece trackPiece = track.getTrackPiece( trackPieceIndex );
                if( DEBUG ) puts( "next switch trackPieceIndex: " + trackPiece.getIndex() );
                switchTrackPieces.put( trackPieceIndex, trackPiece );
            }
        }
        return switchTrackPieces;
    }

    private NavigableMap<Integer,TrackPiece> createTrackPieceMap(List<TrackPiece> pieces) {
        NavigableMap<Integer,TrackPiece> trackPieceMap = new TreeMap<>();
        int i = 0;
        for( TrackPiece trackPiece : pieces ){
            trackPiece.setIndex( i );
            trackPieceMap.put( i, trackPiece );
            i++;
        }
        return trackPieceMap;
    }



}
