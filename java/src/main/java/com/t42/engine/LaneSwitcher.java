package com.t42.engine;

import com.t42.value.*;

import java.util.*;

import static org.boon.Boon.puts;


/**
 *
 * makes decisions whether to switch track or not
 *
 *
 */
public enum LaneSwitcher {

    INSTANCE;

    private TrackDataCruncher trackDataCruncher = TrackDataCruncher.getInstance();
    private TrackPiece switchOnTrackPiece = null;    // upcoming trackpiece where we are making a lane switch
    private int trafficCalculationDistance = 1;
    private boolean DEBUG = false;


    Map<Integer,Map<Lane,NavigableMap<CarId,CarPosition>>> laneTrafficsHistory = new HashMap<>();

    public static LaneSwitcher getInstance(){
        return INSTANCE;
    }

    public void init(){


    }

    public Direction getPreferredSwitchDirection(int tick){

        int trackPieceIndex = trackDataCruncher.getOurCarPosition( tick ).getPiecePosition().getPieceIndex();
        TrackPiece currentTrackPiece = trackDataCruncher.getTrackPiece( trackPieceIndex );
        TrackPiece nextSwitchTrackPiece = trackDataCruncher.getNextSwitch( currentTrackPiece.getIndex() );
        Direction preferredNextSwitchDirection = nextSwitchTrackPiece.getPreferredSwitchDirection();

        // calculate lane traffic
        Map<Lane,NavigableMap<CarId,CarPosition>> laneTraffics = calculateTrafficData( tick, trafficCalculationDistance );
        laneTrafficsHistory.put( tick, laneTraffics );

        Lane currentLane = trackDataCruncher.getOurLane( tick );
        Lane rightLane = getLaneInDirection( Direction.Right, currentLane );
        Lane leftLane = getLaneInDirection( Direction.Left, currentLane );
        Lane preferredLane = getLaneInDirection( preferredNextSwitchDirection, currentLane );

        if( DEBUG ) {
            puts( "LANE -> our car: " + trackDataCruncher.getOurCar().getName() + ", color: " + trackDataCruncher.getOurCar().getColor() + ", pieceindex: " + trackPieceIndex + ", lane: " + currentLane.getIndex() );
            puts( "LANE -> preferredNextSwitchDirection before: " + preferredNextSwitchDirection );

            if( preferredLane != null ){
                puts( "LANE -> Preferred Lane [" + preferredLane.getIndex() + "], " + (hasTrafficAhead( tick, preferredLane ) ? "TRAFFIC AHEAD!" : "NO TRAFFIC") );
            } else {
                puts( "LANE -> Preferred Lane does not exist! -> Preferred Lane = CurrentLane" );
            }
        }

        // fix preferred direction if lane in that direction doesnt exist
        if( preferredLane == null ){
            preferredLane = currentLane;
            preferredNextSwitchDirection = Direction.Straight;
        }

        if( DEBUG ) {
            if (rightLane != null) {
                puts("LANE -> Right Lane     [" + rightLane.getIndex() + "], " + (hasTrafficAhead(tick, rightLane) ? "TRAFFIC AHEAD!" : "NO TRAFFIC"));
            } else {
                puts("LANE -> Right Lane does not exist!");
            }

            if (currentLane != null) {
                puts("LANE -> Current Lane   [" + currentLane.getIndex() + "], " + (hasTrafficAhead(tick, currentLane) ? "TRAFFIC AHEAD!" : "NO TRAFFIC"));
            } else {
                puts("LANE -> Current Lane does not exist!");
            }

            if (leftLane != null) {
                puts("LANE -> Left Lane      [" + leftLane.getIndex() + "], " + (hasTrafficAhead(tick, leftLane) ? "TRAFFIC AHEAD!" : "NO TRAFFIC"));
            } else {
                puts("LANE -> Left Lane does not exist!");
            }
        }
        // preferredDirection is Straight only when left and right turn amounts are equal

        boolean preferredIsCurrent = (currentLane.getIndex() == preferredLane.getIndex()) ? true : false;

        // WE ARE NOT ON PREFERRED LANE
        if( !preferredIsCurrent ){

            if( !hasTrafficAhead( tick, preferredLane ) ){
                // IF PREFERRED HAS NO TRAFFIC -> SWITCH
                if( DEBUG ) puts( "LANE -> NOT ON PREFERRED LANE, PREFERRED HAS NO TRAFFIC, preferredNextSwitchDirection: " + preferredNextSwitchDirection );
            } else {
                // PREFERRED HAS TRAFFIC
                if( hasTrafficAhead( tick, currentLane ) ){
                    // CURRENT HAS TRAFFIC TOO -> choose the one with less
                    Lane lessTrafficLane = compareTrafficsAhead( tick, currentLane, preferredLane );
                    preferredNextSwitchDirection = getLaneDirectionFromCurrent( lessTrafficLane, currentLane );
                    if( DEBUG ) puts( "LANE -> NOT ON PREFERRED LANE, PREFERRED HAS TRAFFIC, CURRENT HAS TRAFFIC, preferredNextSwitchDirection: " + preferredNextSwitchDirection );
                }else{
                    // CURRENT HAS NO TRAFFIC
                    preferredNextSwitchDirection = Direction.Straight;
                    if( DEBUG ) puts( "LANE -> NOT ON PREFERRED LANE, PREFERRED HAS TRAFFIC, CURRENT HAS NO TRAFFIC, preferredNextSwitchDirection: " + preferredNextSwitchDirection );
                }

            }
        } else {
            //
            if( !hasTrafficAhead( tick, preferredLane ) ){
                if( DEBUG ) puts( "LANE -> WE ARE ON PREFERRED LANE, PREFERRED HAS NO TRAFFIC" );
            } else {
                // PREFERRED HAS TRAFFIC
                if( DEBUG ) puts( "LANE -> WE ARE ON PREFERRED LANE, PREFERRED HAS TRAFFIC" );
                // get the other lane
                Lane otherLane = (getLaneInDirection(Direction.Left,preferredLane)!=null) ?
                        getLaneInDirection(Direction.Left,preferredLane):
                        getLaneInDirection(Direction.Right,preferredLane);
                //if( DEBUG ) puts( "LANE -> otherLane: " + otherLane );

                if( hasTrafficAhead( tick, otherLane ) ){
                    // OTHER HAS TRAFFIC TOO -> choose the one with less
                    Lane lessTrafficLane = compareTrafficsAhead( tick, currentLane, otherLane );
                    preferredNextSwitchDirection = getLaneDirectionFromCurrent( lessTrafficLane, currentLane );
                    if( DEBUG ) puts( "LANE -> WE ARE ON PREFERRED LANE, PREFERRED HAS TRAFFIC, OTHER LANE HAS TRAFFIC, preferredNextSwitchDirection: "  + preferredNextSwitchDirection );
                }else{
                    // OTHER HAS NO TRAFFIC
                    if( DEBUG ) puts( "LANE -> currentLane: " + currentLane );
                    preferredNextSwitchDirection = getLaneDirectionFromCurrent( otherLane, currentLane );
                    if( DEBUG ) puts( "LANE -> WE ARE ON PREFERRED LANE, PREFERRED HAS TRAFFIC, OTHER LANE HAS NO TRAFFIC, preferredNextSwitchDirection" + preferredNextSwitchDirection );
                }

            }
        }
        //if( DEBUG ) puts( "LANE -> preferredNextSwitchDirection: " + preferredNextSwitchDirection );

        return preferredNextSwitchDirection;
    }

    /**
     * compares lane traffics and returns lane which has less traffic,
     * if equal, returns the one which traffic is further ahead
     * if no cars on both lanes, returns null
     * @param tick
     * @param lane1
     * @param lane2
     * @return
     */
    public Lane compareTrafficsAhead( int tick, Lane lane1, Lane lane2 ){
        Map<Lane,NavigableMap<CarId,CarPosition>> laneTraffics = laneTrafficsHistory.get(tick);

        NavigableMap<CarId,CarPosition> lane1Traffic = laneTraffics.get(lane1);
        NavigableMap<CarId,CarPosition> lane2Traffic = laneTraffics.get(lane2);

        boolean lane1CarsAhead = hasTrafficAhead( tick, lane1 );
        boolean lane2CarsAhead = hasTrafficAhead( tick, lane2 );

        // no cars
        if( !lane1CarsAhead && !lane2CarsAhead ){
            return null;
        }

        // both have cars ahead --> return the one which has traffic further ahead
        if( lane1CarsAhead && lane2CarsAhead ){
            double lane1ShortestDistance = 10000.0;
            double lane2ShortestDistance = 10000.0;
            for( Map.Entry<CarId,CarPosition> carEntry : lane1Traffic.entrySet() ){
                lane1ShortestDistance = carEntry.getValue().getDistance() < lane1ShortestDistance ?
                        carEntry.getValue().getDistance() : lane1ShortestDistance;
            }
            for( Map.Entry<CarId,CarPosition> carEntry : lane2Traffic.entrySet() ){
                lane2ShortestDistance = carEntry.getValue().getDistance() < lane2ShortestDistance ?
                        carEntry.getValue().getDistance() : lane2ShortestDistance;
            }
            return lane1ShortestDistance < lane2ShortestDistance ? lane2 : lane1;
        }
        return lane1CarsAhead ? lane2 : lane1;
    }

    public boolean hasTrafficAhead( int tick, Lane lane ){
        Map<Lane,NavigableMap<CarId,CarPosition>> laneTraffics = laneTrafficsHistory.get( tick );
        NavigableMap<CarId,CarPosition> laneTraffic = laneTraffics.get(lane);
        int laneCarsAhead = getNumberOfCarsAhead(laneTraffic);
        return laneCarsAhead > 0 ? true : false;
    }

    /**
     * returns amount of cars ahead in given lane traffic map
     * @param laneTraffic
     */
    private int getNumberOfCarsAhead(NavigableMap<CarId, CarPosition> laneTraffic){
        int count = 0;
        for( Map.Entry<CarId,CarPosition> carEntry : laneTraffic.entrySet() ){
            if( carEntry.getValue().isAhead() ) {
                count++;
            }
        }
        return count;
    }

    /**
     *
     * return opposite direction of given direction, straight if straight.
     * @param direction
     * @return
     */
    public Direction getOppositeDirection( Direction direction ){
        if( direction.equals( Direction.Straight ) ){
            return Direction.Straight;
        }
        return direction.equals( Direction.Left ) ? Direction.Right : Direction.Left;
    }

    /**
     * returns the lane in given direction from our current lane
     * @param direction
     * @return
     */
    public Lane getLaneInDirection( Direction direction, Lane ourLane ){
        if( direction.equals( Direction.Straight ) ){
            return ourLane;
        }
        return direction.equals( Direction.Left ) ?
                trackDataCruncher.getTrack().getLeftLane( ourLane ) :
                trackDataCruncher.getTrack().getRightLane( ourLane );
    }

    public Direction getLaneDirectionFromCurrent( Lane unknownLane, Lane currentLane ){

        if( currentLane.getIndex() == unknownLane.getIndex() ){
            return Direction.Straight;
        }

        Lane leftLane = trackDataCruncher.getTrack().getLeftLane( currentLane );
        if( leftLane == null ){
            return Direction.Right;
        }
        return leftLane.getIndex() == unknownLane.getIndex() ?
                Direction.Left :
                Direction.Right;
    }

    /**
     *
     * returns true if carposition is inside given distance and ahead from the given position
     *
     * @param from
     * @param to
     * @param distance
     * @return
     */
    public boolean isAhead( TrackPiecePosition from, TrackPiecePosition to, int distance ) {

        if( from.getPieceIndex() == to.getPieceIndex() ){
            if( from.getInPieceDistance() < to.getInPieceDistance() ){
                return true;
            }
        } else {
            for( int i=0; i <= distance; i++ ){
                TrackPiece trackPieceAhead = trackDataCruncher.getTrackPieceAhead( trackDataCruncher.getTrackPiece( from.getPieceIndex() ), i );
                if( trackPieceAhead.getIndex() == to.getPieceIndex() ){
                    return true;
                }
            }
        }
        return false;
    }

    public double getDistance( TrackPiecePosition from, TrackPiecePosition to, boolean ahead ) {

        if( from.getPieceIndex() == to.getPieceIndex() ){
            double distance = to.getInPieceDistance()-from.getInPieceDistance();
            puts( "getDistance SAME PIECE: " + distance );
            return distance;

        } else {
            if( ahead ){

                int currentIndex = 0;
                double distance = trackDataCruncher.getTrackPieceDistanceLeft( from, trackDataCruncher.getTrack().getLane( from.getLane().getEndLaneIndex() ) );
                //puts( "getDistance DIFFERENT PIECE, trackpiece: " + from.getPieceIndex() + ", trackpieceDistanceLeft: " + distance );
                //puts( "getDistance DIFFERENT PIECE, current distance: " + distance );

                for( int i=1; currentIndex != to.getPieceIndex(); i++ ){
                    TrackPiece trackPiece = trackDataCruncher.getTrackPieceAhead( trackDataCruncher.getTrackPiece( from.getPieceIndex() ), i );
                    currentIndex = trackPiece.getIndex();
                    //puts( "getDistance DIFFERENT PIECE, i: " + i + ", on trackpiece: " + currentIndex );

                    double distanceToAdd = 0.0;
                    if( currentIndex != to.getPieceIndex() ){
                        if( trackPiece.isCurve() ){
                            distanceToAdd = trackPiece.getCurveLength( trackDataCruncher.getTrack().getLane( from.getLane().getEndLaneIndex() ), trackDataCruncher.getTrack().getTrackDirection() );
                        } else {
                            distanceToAdd = trackPiece.getLength();
                        }
                        //puts( "getDistance TARGET NOT ON THIS PIECE, trackpiece: " + currentIndex + ", distanceToAdd: " + distanceToAdd );
                        distance = distance + distanceToAdd;
                        //puts( "getDistance TARGET NOT ON THIS PIECE, current distance: " + distance );
                    }else{
                        distanceToAdd =  to.getInPieceDistance();
                        //puts( "getDistance TARGET ON THIS PIECE, piece: " + currentIndex + ", add inPieceDistance: " + distanceToAdd );
                        distance = distance + distanceToAdd;
                    }
                }
                return distance;
            } else {

                // todo:
                return -1000;
            }

        }
    }


    public boolean canSwitchLane( Direction direction, Lane currentLane ){
        Lane lane = getLaneInDirection( direction, currentLane );
        return lane != null ? true : false;
    }

    public TrackPiece getSwitchOnTrackPiece(){
        return switchOnTrackPiece;
    }

    public void setSwitchOnTrackPiece( TrackPiece trackPiece ){
        this.switchOnTrackPiece = trackPiece;
    }

    public boolean isSwitchScheduled( int tick ){
        int currentTrackPieceIndex = trackDataCruncher.getOurCarPosition( tick ).getPiecePosition().getPieceIndex();

        // check if we have moved past the scheduled switch
        if( switchOnTrackPiece != null && currentTrackPieceIndex > switchOnTrackPiece.getIndex() ){
            switchOnTrackPiece = null;
        }

        return (switchOnTrackPiece != null) ? true : false;
    }

    /**
     * calculates Traffic inside given distance
     *
     * @param trafficCalculationDistance
     */
    private Map<Lane,NavigableMap<CarId,CarPosition>> calculateTrafficData( int tick, int trafficCalculationDistance ) {

        if( DEBUG ) puts( "TRAFFICDATA, trafficCalculationDistance: " + trafficCalculationDistance );
        //Lane currentLane = trackDataCruncher.getOurLane(tick);
        Map<Lane,NavigableMap<CarId,CarPosition>> laneTraffics = new HashMap<>();

        for( Lane lane : trackDataCruncher.getTrack().getLanes() ){
            laneTraffics.put( lane, new TreeMap<CarId,CarPosition>() );
        }

        CarPosition ourCarPosition = trackDataCruncher.getOurCarPosition( tick );
        if( DEBUG ) puts( "OUR CAR: trackPiece: " + ourCarPosition.getPiecePosition().getPieceIndex() + ", inDistance: " + ourCarPosition.getPiecePosition().getInPieceDistance() );

        int lastTrackPieceIndex = trackDataCruncher.getTrack().getLastTrackPiece().getIndex();

        for( CarId competitorCarId : trackDataCruncher.getCompetitorCars() ){
            CarPosition competitorCarPosition = trackDataCruncher.getCompetitorCarPosition( tick, competitorCarId );

            if( competitorCarPosition.isInDistanceOf( ourCarPosition.getPiecePosition(), trafficCalculationDistance, lastTrackPieceIndex ) ){
                if( DEBUG ) puts( "COMPETITOR " + competitorCarId.getName() + ": trackPiece: " + competitorCarPosition.getPiecePosition().getPieceIndex() + ", inDistance: " + competitorCarPosition.getPiecePosition().getInPieceDistance() );

                if( isAhead( trackDataCruncher.getOurCarPosition( tick ).getPiecePosition(),
                        competitorCarPosition.getPiecePosition(),
                        trafficCalculationDistance ) ){

                    competitorCarPosition.setAhead( true );
                    double distance = getDistance( ourCarPosition.getPiecePosition(), competitorCarPosition.getPiecePosition(), true );
                    competitorCarPosition.setDistance( distance );

                    if( DEBUG ) puts( "CARDATA car: " + competitorCarId.getName() + ", ahead: " + competitorCarPosition.isAhead() + ", distance: " + distance + ", trackposition: " + competitorCarPosition.getPiecePosition() );
                } else {
                    competitorCarPosition.setAhead( false );

                    // todo: calculate behind distance

                }
                // car is part of traffic
                int competitorCarLaneIndex = competitorCarPosition.getPiecePosition().getLane().getEndLaneIndex();
                Lane competitorCarLane = trackDataCruncher.getTrack().getLane( competitorCarLaneIndex );
                NavigableMap<CarId,CarPosition> laneTraffic = laneTraffics.get( competitorCarLane );
                laneTraffic.put( competitorCarId, competitorCarPosition );
            }
        }

        //--
        return laneTraffics;
    }

    private Map<CarId,Double> getCompetitorSpeeds( int tick ){

        Map<CarId,Double> competitorSpeeds = new HashMap<>();
        for( CarId carId : trackDataCruncher.getCompetitorCars() ){
            competitorSpeeds.put( carId, trackDataCruncher.getCompetitorSpeed( tick, carId ) );
        }
        return competitorSpeeds;
    }

    private Map<CarId,CarPosition> getCompetitorPositions( int tick ){
        Map<CarId,CarPosition> competitorPositions = new HashMap<>();
        for( CarId carId : trackDataCruncher.getCompetitorCars() ){
            competitorPositions.put( carId, trackDataCruncher.getCompetitorCarPosition(tick, carId) );
        }
        return competitorPositions;
    }

}
