package com.t42.engine;

/**
 * Created by Ville on 27.4.2014.
 */
public class MathHelper {
    public static double roundToOneDecimals(double value) {
        return Math.round(value*10.0)/10.0;
    }
}
