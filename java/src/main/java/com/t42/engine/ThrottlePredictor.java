package com.t42.engine;

import static org.boon.Boon.puts;

import com.t42.value.*;

/**
 *
 * Created by Ville on 15.4.2014.
 */
public enum ThrottlePredictor {

    INSTANCE;

    public static ThrottlePredictor getInstance(){
        return INSTANCE;
    }

    private double lastDriftAngle = 0;

    //learn and fix throttle up/down
    private RaceSession raceSession;
    private double drag;
    private double v1 = 0;
    private double v2 = 0;
    private double v3 = 0;
    private double mass = 5.0;
    private final double initialCalcThrottle = 1.0;
    private double friction =0.42;

    private boolean frictionSet = false;
    private boolean dragAndMassSet = false;
    private final double minThrottle = 0.00;
    private final double maxThrottle = 1.00;
    private double throttle = maxThrottle;
    private boolean DEBUG = false;
    private double maxDriftAngle = 50.00;
    private double maxDriftThrottle = 0.10;

    public void init() {
    }

    public double getEndingThrottleForTick( int tick, int lap ) {
        TrackDataCruncher tdc = TrackDataCruncher.getInstance();
        try {
            CarPosition ourCarPosition = tdc.getOurCarPosition(tick);
            DistanceToNextKeyPiece distance = tdc.getDistanceToNextKeyPiece(tick);
            if  (DEBUG) {
                puts( "DISTANCE TO NEXT KEY PIECE: " + distance.getDistance() );
            }
            TrackPiece currentTrackPiece = tdc.getTrack().getTrackPiece(ourCarPosition.getPiecePosition().getPieceIndex());
            double currentSpeed = tdc.getOurSpeed(tick);
            //On start full speed!
            if ( tick > 10 && frictionSet) { //TODO: start and end throttle handling
                calculateThrottle(distance, currentSpeed, lap, ourCarPosition, tdc.getOurLane(tick), currentTrackPiece);
            } else if (!dragAndMassSet) {
                calculateDragAndMass(tick, currentSpeed);
            }

            double ourDriftAngle = Math.abs(ourCarPosition.getAngle());
            double driftAngleChange = ourDriftAngle - lastDriftAngle;

            if (ourDriftAngle > 0.0 && driftAngleChange > 0 && !frictionSet) {
                calculateFriction(tdc, ourCarPosition, currentTrackPiece, currentSpeed);
                PredictMaxSpeedsForPieces(tdc);
            }

            if ( ( ourDriftAngle > (maxDriftAngle - 10) || ( ourDriftAngle > lastDriftAngle && driftAngleChange > 4 )) && currentSpeed > 1.0 ) {
                    throttle = minThrottle;
            }

            lastDriftAngle = ourDriftAngle;
        } catch (Exception e) {
            puts("smth odd happened while calculating throttle");
        }
        sanityCheckThrottle();
        return throttle;
    }

    private void PredictMaxSpeedsForPieces(TrackDataCruncher tdc) {
        //CALCULATE RAW PREDICTIONS
        for ( TrackPiece trackPiece : tdc.getTrack().getTrackPieces().values()) {
            if ( DEBUG ) {
                puts("----------------------------------------" );
                puts("TRACKPIECE MAX SPEEDS: " + trackPiece.getIndex());
            }
            for ( Lane lane : tdc.getTrack().getLanes()) {
                if ( trackPiece.isCurve()) {
                    double actualRadius = calculateActualRadius(trackPiece, lane);
                    if ( DEBUG ) {
                        puts("LANE MAX SPEEDS: " + lane.getIndex());
                        puts("radius: " + trackPiece.getRadius());
                        puts("direction: " + trackPiece.getCurveDirection());
                        puts("lane distance from center: " + lane.getDistanceFromCenter());
                        puts("actual radius: " + actualRadius);
                    }

                    trackPiece.setMaxSpeedForLane(lane.getIndex(), calcMaxVelocityBeforeSlipping(actualRadius));
                } else {
                    trackPiece.setMaxSpeedForLane(lane.getIndex(), (calculateTerminalVelocityForThrottle(1.0) +5)); //TODO ennakointi?
                }
                if ( DEBUG ) {
                    puts("lane: " + lane.getIndex() + " Max speed dynamic: " + trackPiece.getMaxSpeedForLane(lane.getIndex()));
                }
            }
        }
    }

	private double calculateActualRadius(TrackPiece trackPiece, Lane lane) {
		if (trackPiece.getCurveDirection() == Direction.Left) {
			return trackPiece.getRadius() + lane.getDistanceFromCenter();
		} else if (trackPiece.getCurveDirection() == Direction.Right) {
			return trackPiece.getRadius() - lane.getDistanceFromCenter();			
		}
		return trackPiece.getRadius();
	}

    private void calculateFriction(TrackDataCruncher tdc, CarPosition ourCarPosition, TrackPiece currentTrackPiece, double currentSpeed) {
        Lane currentLane = tdc.getTrack().getLane(ourCarPosition.getPiecePosition().getLane().getStartLaneIndex());
        double actualCurrentRadius = calculateActualRadius(currentTrackPiece, currentLane); 
        friction = calculateFrictionBeforeSlipping(currentSpeed, ourCarPosition.getAngle(), actualCurrentRadius);
        if ( DEBUG ) {
            puts("-----------------FRICTION-----------------------" );
            puts("currentlane " + currentLane.getIndex(), " distance: " + currentLane.getDistanceFromCenter());
            puts("Current trackpiece radius: " + currentTrackPiece.getRadius() + " actualCurrentRadius " + actualCurrentRadius);
            puts("FRICTION: " + friction);
        }
        frictionSet = true;
    }

    private double calculateFrictionBeforeSlipping(double currentSpeed, double angle, double radius) {
        double angV =Math.toDegrees(currentSpeed/radius);
        double maxAngVBeforSlip = angV - Math.abs( angle );
        double speed = Math.toRadians(maxAngVBeforSlip ) * radius;
        return speed * speed / radius;
    }

    private double calcMaxVelocityBeforeSlipping(double radius) {
        return Math.sqrt(friction * (radius));
    }

    private void calculateThrottle(DistanceToNextKeyPiece distance, double currentSpeed, int lap, CarPosition ourCarPosition, Lane lane, TrackPiece currentTrackPiece) {
        switch (distance.getChangePartType()) {
            case STRAIGHT_TO_CURVE:
                calcStraightToCurve(distance, currentSpeed, ourCarPosition);
                break;
            case CURVE_TO_STRAIGHT:
                calcCurveToStraight(distance, currentSpeed, ourCarPosition);
                break;
            case CURVE_TO_CURVE:
                calcCurveToCurve(distance, currentSpeed, ourCarPosition);
                break;
            default:
                break;
        }
    }

    private void calcCurveToCurve(DistanceToNextKeyPiece distance, double currentSpeed, CarPosition ourCarPosition) {
        TrackPiece nextTargetPiece = distance.getToPiece();
        double targetSpeed = nextTargetPiece.getMaxSpeedForLane(ourCarPosition.getPiecePosition().getLane().getStartLaneIndex());
        if ( distance.getFromPiece().getRadius() > distance.getToPiece().getRadius() ) {
            double breakDistance = calculateBreakDistance(currentSpeed, targetSpeed, minThrottle);
            double distanceBufferTicks = calculateDistanceInTicks( currentSpeed, 1,  0.00, throttle );//ONE TICK AHEAD PREDICTION
            breakDistance = breakDistance + distanceBufferTicks;
            if ( DEBUG ) {
                puts("BREAKDISTANCE: " + breakDistance);
            }
            if ( (breakDistance) >= distance.getDistance() && currentSpeed > targetSpeed ) {
                throttle = minThrottle;
            } else {
                throttle = calculateThrottleForVelocity( distance.getFromPiece().getMaxSpeedForLane(ourCarPosition.getPiecePosition().getLane().getStartLaneIndex()) );
                addDriftThrottle();
            }
        } else {
            throttle = calculateThrottleForVelocity( distance.getFromPiece().getMaxSpeedForLane(ourCarPosition.getPiecePosition().getLane().getStartLaneIndex()) );
            addDriftThrottle();
        }
    }

    private void calcCurveToStraight(DistanceToNextKeyPiece distance, double currentSpeed, CarPosition ourCarPosition) {
        int ourLaneIndex = ourCarPosition.getPiecePosition().getLane().getStartLaneIndex();
        double breakDistance = 0.00;
        double breakDistanceNextCurve = calculateBreakDistance(distance.getFromPiece().getMaxSpeedForLane(ourLaneIndex), distance.getKeyPiece2().getToPiece().getMaxSpeedForLane(ourLaneIndex), minThrottle);
        if ( distance.getToPiece().isStraight()
                && breakDistanceNextCurve > 0
                && breakDistanceNextCurve > distance.getKeyPiece2().getDistance()
                && currentSpeed > distance.getKeyPiece2().getToPiece().getMaxSpeedForLane(ourLaneIndex)) {
            breakDistance = breakDistanceNextCurve - distance.getKeyPiece2().getDistance();
        }
        if ( breakDistance > distance.getDistance() ) {
            throttle = minThrottle;
        } else {
            throttle = calculateThrottleForVelocity( distance.getFromPiece().getMaxSpeedForLane(ourCarPosition.getPiecePosition().getLane().getStartLaneIndex()) );
            addDriftThrottle();
        }
    }

    private void addDriftThrottle() {
        if (throttle <= (maxThrottle - maxDriftThrottle)) {
            throttle = throttle + maxDriftThrottle;
        } else {
            throttle = maxThrottle;
        }
    }

    private void calcStraightToCurve(DistanceToNextKeyPiece distance, double currentSpeed, CarPosition ourCarPosition) {
        TrackPiece nextTargetPiece = distance.getToPiece();
        int ourLaneIndex = ourCarPosition.getPiecePosition().getLane().getStartLaneIndex();
        double targetSpeed = nextTargetPiece.getMaxSpeedForLane(ourLaneIndex);
        double breakDistance = calculateBreakDistance(currentSpeed, targetSpeed, minThrottle);
        double distanceBufferTicks = calculateDistanceInTicks( currentSpeed, 1,  0.00, throttle );//ONE TICK AHEAD PREDICTION
        breakDistance = breakDistance + distanceBufferTicks;
        if ( DEBUG ) {
            puts("BREAKDISTANCE: " + breakDistance);
        }

        //CALC NEXT KEY PIECE BRAKE DISTANCE USING TARGET SPEED
        breakDistance = calcAdditionalBreakDistance(ourLaneIndex, breakDistance, distance.getKeyPiece2(), currentSpeed);
        if ( DEBUG ) {
            puts("BREAKDISTANCE2: " + breakDistance);
        }
//        breakDistance = calcAdditionalBreakDistance(ourLaneIndex, breakDistance, distance.getKeyPiece3(), currentSpeed);
//
//        if ( DEBUG ) {
//            puts("FINAL BREAKDISTANCE: " + breakDistance);
//        }
        if ( breakDistance >= distance.getDistance() ) {
            throttle = minThrottle;
        } else {
            throttle = maxThrottle;
        }
    }

    private double calcAdditionalBreakDistance(int ourLaneIndex, double breakDistance, DistanceToNextKeyPiece keyPiece, double currentSpeed) {
        double breakDistance2 = calculateBreakDistance( keyPiece.getFromPiece().getMaxSpeedForLane(ourLaneIndex), keyPiece.getToPiece().getMaxSpeedForLane(ourLaneIndex), minThrottle );
        if ( keyPiece.getToPiece().isCurve()
                && breakDistance2 > 0
                && breakDistance2 > (breakDistance + keyPiece.getDistance())
                && currentSpeed > keyPiece.getToPiece().getMaxSpeedForLane(ourLaneIndex)) {
            double additionalBreakDistance = breakDistance2 - keyPiece.getDistance();
            breakDistance = breakDistance + additionalBreakDistance;
        }
        return breakDistance;
    }

//    private double calcAdditionalBreakDistance2(int ourLaneIndex, double breakDistance, DistanceToNextKeyPiece keyPiece, double currentSpeed) {
//        double breakDistance2 = calculateBreakDistance( keyPiece.getFromPiece().getMaxSpeedForLane(ourLaneIndex), keyPiece.getToPiece().getMaxSpeedForLane(ourLaneIndex), minThrottle );
//        if ( breakDistance2 > (breakDistance + keyPiece.getDistance()) && currentSpeed > keyPiece.getToPiece().getMaxSpeedForLane(ourLaneIndex)) {
//            double additionalBreakDistance = breakDistance2 - keyPiece.getDistance();
//            breakDistance = breakDistance + additionalBreakDistance;
//        }
//        return breakDistance;
//    }

    private double calculateBreakDistance(double currentSpeed, double targetSpeed, double breakThrottle) {
        int ticksToTargetSpeed = calculateAmountOfTicksToTargetSpeed(targetSpeed, currentSpeed, breakThrottle);
        return calculateDistanceInTicks( currentSpeed, ticksToTargetSpeed,  0.00, breakThrottle );
    }

    /**
     * we don't want to send negative throttles...
     * @return
     */
    private void sanityCheckThrottle() {
        if ( Double.isNaN(throttle) || throttle > maxThrottle || throttle < minThrottle ) {
            throttle = 0.6;
        }
    }

    private int mdtick1 = 0;
    private int mdtick2 = 0;
    private int mdtick3 = 0;
    private void calculateDragAndMass(int tick, double currentSpeed) {
        if ( tick > 0) {
            if ( currentSpeed > 0 && v1 == 0 ) {
                mdtick1 = tick;
                mdtick2 = tick +1;
                mdtick3 = tick +2;
            }
            if (mdtick1 == tick) {
                v1 = currentSpeed;
            }
            if (mdtick2 == tick) {
                v2 = currentSpeed;
            }
            if (mdtick3 == tick) {
                v3 = currentSpeed;
                drag = (v1 - ( v2 - v1 ) ) / Math.pow(v1, 2);
                mass = 1.0 / ( Math.log( ( v3 - ( initialCalcThrottle / drag ) ) / ( v2 - ( initialCalcThrottle / drag ) ) ) / ( -drag ) );
                dragAndMassSet = true;
                if (DEBUG) {
                    puts("V1 " + v1);
                    puts("V2 " + v2);
                    puts("V3 " + v3);
                    puts("DRAG " + drag);
                    puts("MASS " + mass);
                    puts("1.0 Terminalvelocity " + calculateTerminalVelocityForThrottle(1.0));
                    puts("5.0 Terminalvelocity " + calculateTerminalVelocityForThrottle(0.5));
                }
            }
        }
    }

    private double calculateTerminalVelocityForThrottle(double throttle) {
        return throttle / drag;
    }

    private double calculateThrottleForVelocity(double speed) {
        return speed * drag;
    }

    private double calculateSpeedAfterTicks(double throttle, double currentSpeed, int ticks ) {
        return (currentSpeed - (throttle/drag) ) * Math.exp ( ( - drag * ticks ) / mass ) + ( throttle/drag );
    }

    private int calculateAmountOfTicksToTargetSpeed(double targetSpeed, double currentSpeed, double throttle) {
        return (int) Math.round( (Math.log( (targetSpeed - ( throttle/drag ) )/(currentSpeed - ( throttle/drag ) ) ) * mass ) / ( -drag ));
    }

    private double calculateDistanceInTicks(double currentSpeed, int ticks, double currentPosition, double throttle) {
        return ( mass/drag ) * ( currentSpeed - ( throttle/drag ) ) * ( 1.0 - Math.exp ( ( -drag*ticks ) / mass ) ) + ( throttle/drag ) * ticks + currentPosition;
    }

    /**
     * Use this to reset current throttle to maximum throttle and get throttle
     * @return
     */
    public double resetCurrentThrottle() {
        throttle = maxThrottle;
        if ( maxDriftThrottle > 0.02 ) {
            maxDriftThrottle = maxDriftThrottle - 0.04;
            if ( maxDriftThrottle < 0 ) {
                maxDriftThrottle = 0.02;
            }
        }
        return throttle;
    }


    public void resetFriction() {
        frictionSet = false;
    }
}
