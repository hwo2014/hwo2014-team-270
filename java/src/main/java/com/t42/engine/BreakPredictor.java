package com.t42.engine;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import static org.boon.Boon.puts;

/**
 *
 * Created by Juha on 26.4.2014.
 */
public enum BreakPredictor {

    INSTANCE;

    public static BreakPredictor getInstance(){
        return INSTANCE;
    }

    private double BREAK_TEST_SPEED_TARGET = 1.1;   // target speed to reach to start testing
    private double BREAK_TEST_SPEED_STEP = 0.1;     // value that's added to break test speed limit after each test
    private boolean BREAKING = false;

    private boolean DEBUG = false;

//    private DecimalFormat decimalFormat = new DecimalFormat("#.#");

    private TrackDataCruncher trackDataCruncher = TrackDataCruncher.getInstance();
    private LaneSwitcher laneSwitcher = LaneSwitcher.getInstance();
    private ThrottlePredictor throttlePredictor = ThrottlePredictor.getInstance();
    private TurboActivator turboActivator = TurboActivator.getInstance();

    // speedDecelerationMap contains speed and distance needed to decelerate to that speed
    private NavigableMap<Double,Double> speedDecelerationMap = new TreeMap<>();

    private NavigableMap<Integer,Double> throttleMap = new TreeMap<>();
    private NavigableMap<Integer,Double> speedHistoryMap = new TreeMap<>();


    public void init( double initialBreakTestSpeedTarget, double breakTestSpeedStep ) {
        BREAK_TEST_SPEED_TARGET = initialBreakTestSpeedTarget;
        BREAK_TEST_SPEED_STEP = breakTestSpeedStep;
    }

    /**
     * saves breaking data and controls the breaking value which is used to control data gathering during qualifying
     *
     * in addition saves throttle and speed history per tick
     *
     * @param tick
     * @param currentThrottle
     */
    public void saveData( int tick, double currentThrottle ) {

        double previousThrottle = -1.0;
        double currentSpeed = trackDataCruncher.getOurSpeed(tick);

        puts( "currentSpeed: " + currentSpeed );
        // first set new break speed target if speedDecelerationMap already contains the current speed
        if( ( currentSpeed < 1.0 || speedDecelerationMap.containsKey( MathHelper.roundToOneDecimals(currentSpeed)  ) )
                && BREAKING ){
            BREAKING = false;   // start speeding up again
            BREAK_TEST_SPEED_TARGET = BREAK_TEST_SPEED_TARGET + BREAK_TEST_SPEED_STEP; // target up by one step
        }

        // start breaking again when we reach current break speed target
        if( currentSpeed > BREAK_TEST_SPEED_TARGET ){
            BREAKING = true;
        }

        if( throttleMap.containsKey( tick-1 ) ){
            previousThrottle = throttleMap.get( tick -1 );
        }

        throttleMap.put( tick, currentThrottle );
        speedHistoryMap.put( tick, currentSpeed );

        // breaking -> save data
        // jos ollaan jarrutettu tickin ajan, tallennetaan jarrutuksen aikana kuljettu matka (previousSpeed) speedDecelationMap:iin
        if( previousThrottle == 0.0 ){
            double previousSpeed = speedHistoryMap.get( tick-1 );

            // TODO: check and discard value if distance differs a lot from nearby values
            speedDecelerationMap.put( MathHelper.roundToOneDecimals(currentSpeed), previousSpeed );

            if( DEBUG ) puts( "DECELERATION from: " + MathHelper.roundToOneDecimals(previousSpeed) +
                    " to: " + MathHelper.roundToOneDecimals(currentSpeed) + " needed distance: " +
                  speedDecelerationMap.get( MathHelper.roundToOneDecimals(currentSpeed) ) );
        }


    }

    /**
     *
     * Returns distance needed to break from speed to another
     *
     * @param fromSpeed
     * @param toSpeed
     * @return
     */
    public double getBreakDistance( double fromSpeed, double toSpeed ){

        fromSpeed = MathHelper.roundToOneDecimals(fromSpeed);
        toSpeed = MathHelper.roundToOneDecimals(toSpeed);

        if( DEBUG ) puts( "getBreakDistance, toSpeed: " + toSpeed + ", fromSpeed: " + fromSpeed );
        double breakDistance = 0.0;
        for( double speedKey=toSpeed; speedKey <= fromSpeed; speedKey=MathHelper.roundToOneDecimals(speedKey + 0.1) ){

            // generate keys until they exist
            while( !speedDecelerationMap.containsKey( speedKey ) ){
                Double firstExistingSpeedKey = speedDecelerationMap.ceilingKey( speedKey );
                Double nextExistingSpeedKey = speedDecelerationMap.higherKey( firstExistingSpeedKey );
                if( nextExistingSpeedKey == null ){
                    nextExistingSpeedKey = firstExistingSpeedKey;
                }
                double speedDecelerationDifferenceValue = speedDecelerationMap.get( nextExistingSpeedKey ) - speedDecelerationMap.get( firstExistingSpeedKey );
                double valueToCreateKey = MathHelper.roundToOneDecimals(firstExistingSpeedKey - 0.1) ;
                double valueToCreateValue = speedDecelerationMap.get( firstExistingSpeedKey ) - speedDecelerationDifferenceValue;
                speedDecelerationMap.put( valueToCreateKey, valueToCreateValue );
            }
            breakDistance = breakDistance + speedDecelerationMap.get( speedKey );
        }

        return breakDistance;
    }

    public void printSpeedDecelerationMap(){
        getBreakDistance( speedDecelerationMap.lastEntry().getKey(), 1.0 );
        for( Map.Entry<Double,Double> speedEntry : speedDecelerationMap.entrySet() ){
            if( DEBUG ) puts( speedEntry.getKey() + " " + speedEntry.getValue() );
        }
    }

    public double getBreakSpeedTarget(){
        return BREAK_TEST_SPEED_TARGET;
    }

    public boolean shouldWeBeBraking(){
        return BREAKING;
    }


}
