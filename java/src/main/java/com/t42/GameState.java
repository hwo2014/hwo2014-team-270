package com.t42;

public enum GameState {

    DISCONNECTED( "DISCONNECTED" ),
    JOINED( "JOINED" ),

    QUALIFYING( "QUALIFYING" ),
    RACE( "RACE" ),

    GAME_ENDED( "GAME_ENDED" );

    //CAR_ASSIGNED( "CAR_ASSIGNED" ),
    //GAME_INITED( "GAME_INITED" ),
    //GAME_STARTED( "GAME_STARTED" ),
    //QUALIFYING( "QUALIFYING" ),
    //CRASHED( "CRASHED" ),
    //DNF( "DNF" ),
    //FINISH( "FINISH" ),
    //TOURNAMENT_ENDED( "TOURNAMENT_ENDED" );

    private final String label;

    private GameState(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static GameState fromString( String label ) {
        if (label != null) {
            for( GameState type : GameState.values() ) {
                if( label.equalsIgnoreCase( type.label ) ) {
                    return type;
                }
            }
        }
        return null;
    }

    public String toString() {
        return this.label;
    }
}
