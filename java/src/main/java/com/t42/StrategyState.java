package com.t42;

public enum StrategyState {

    ACCELERATE( "ACCELERATE" ),
    SLOW_DOWN( "SLOW_DOWN" ),
    OVERTAKE( "OVERTAKE" ),
    BLOCK( "BLOCK" );


    private final String label;

    private StrategyState(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static StrategyState fromString( String label ) {
        if (label != null) {
            for( StrategyState type : StrategyState.values() ) {
                if( label.equalsIgnoreCase( type.label ) ) {
                    return type;
                }
            }
        }
        return null;
    }

    public String toString() {
        return this.label;
    }
}
