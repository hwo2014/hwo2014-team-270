package com.t42;


import com.t42.engine.*;
import com.google.gson.Gson;
import com.t42.messages.*;
import com.t42.messages.bot.*;
import com.t42.messages.server.*;
import com.t42.value.*;

import lombok.Data;

import org.boon.json.JsonFactory;
import org.boon.json.ObjectMapper;

import java.io.*;
import java.net.Socket;
import java.text.DecimalFormat;

import static org.boon.Boon.puts;

@Data
public class T42Bot extends MessageType {

    public static void main(String... args) throws IOException {

        // more servers
        // hakkinen.helloworldopen.com (R2)
        // senna.helloworldopen.com (R1)
        // webber.helloworldopen.com (R3)

        // tracks
        // finland, germany, usa, france

        String host      = args.length >= 1 && args[0] != null ? args[0]                   : "testserver.helloworldopen.com";
        int port         = args.length >= 2 && args[1] != null ? Integer.parseInt(args[1]) : 8091;
        String botName   = args.length >= 3 && args[2] != null ? args[2]                   : "T42";
        String botKey    = args.length >= 4 && args[3] != null ? args[3]                   : "A5dIsRq92sm0sA";
        String trackName = args.length >= 5 && args[4] != null ? args[4]				   : null;
        String password  = args.length >= 6 && args[5] != null ? args[5]				   : null;
        int carCount     = args.length >= 7 && args[6] != null ? Integer.parseInt(args[6]) : 1;
        boolean joinRace = args.length >= 8 && args[7] != null ? true					   : false;

        boolean createPrivateRace = trackName != null && password != null && !joinRace;

        puts( "trackName: " + trackName );
        puts( "password: " + password );
        puts( "joinRace: " + joinRace );
        puts( "createPrivateRace: " + createPrivateRace );
        puts( "carCount: " + carCount );
        puts( "joinRace: " + joinRace );

        puts("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        puts( "Private race " + createPrivateRace + ", joinRace = " + joinRace);

        final Socket socket = new Socket(host, port);
        socket.setTcpNoDelay(true); // should be useful for latency?
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        if (createPrivateRace) {
            puts("Creating private race with track = " + trackName + " and carCount = " + carCount);
            new T42Bot( reader, writer, new CreateRace(botName, botKey, trackName, password, carCount));
        } else if (joinRace) {
            puts("Joining private race with track = " + trackName + " and carCount = " + carCount);
            new T42Bot( reader, writer, new JoinRace(botName, botKey, trackName, password, carCount));
        } else {
            new T42Bot( reader, writer, new Join( botName, botKey ) );
        }
    }

    final Gson gson = new Gson();
    private ObjectMapper mapper = JsonFactory.create();
    private PrintWriter writer;

    private boolean DEBUG = false;                  // controls logging
    private boolean USE_TURBO = true;              // disables turboactivator if falce
    private boolean SWITCH_LANES = true;		   // disables laneswitcher if false
    private double SAFE_LANE_SWITCH_SPEED = 9.0;   // max speed for lane switching

    private boolean turboAvailable = false;

    //private GameInit gameInit;
    private YourCar yourCar;

    private GameState currentGameState = GameState.DISCONNECTED;
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");

    private TrackDataCruncher tdc = TrackDataCruncher.getInstance();
    private LaneSwitcher laneSwitcher = LaneSwitcher.getInstance();
    private ThrottlePredictor throttlePredictor = ThrottlePredictor.getInstance();
    //    private BreakPredictor breakPredictor = BreakPredictor.getInstance();
    private TurboActivator turboActivator = TurboActivator.getInstance();
    private int ourCrashTimes = 0;

    public T42Bot( final BufferedReader reader, final PrintWriter writer, final Message join ) throws IOException {

        this.writer = writer;
        String line = null;

        double throttle = 1.00;
        //double previousThrottle = 0.67;


        int lap = 0;
        boolean crashed = false;

        int lastSwitchPieceActedOn = 0; // id for switchpiece for which we have allready sent the switch message
        // join race or create private
        send( join );

        while( ( line = reader.readLine()) != null ) {
            long tickStartTime = System.currentTimeMillis();
            //puts( "got message: " + line );
            //Message message = mapper.fromJson( line, Message.class );
            Message message = gson.fromJson(line, Message.class);


            switch ( message.getMsgType() ){

                case CREATE_RACE:
                    logTickAndMsgType(message);
                    setCurrentGameState( GameState.JOINED );
                    break;

                case JOIN_RACE:
                    logTickAndMsgType(message);
                    setCurrentGameState( GameState.JOINED );
                    send( new Ping(message.getGameTick()) );
                    break;

                case JOIN:
                    logTickAndMsgType(message);
                    setCurrentGameState( GameState.JOINED );
                    break;

                case GAME_STARTS_IN:
                    logTickAndMsgType(message);
                    break;

                case YOUR_CAR:
                    logTickAndMsgType(message);
                    yourCar = mapper.fromJson( line, YourCar.class);
                    break;

                case GAME_INIT:
                    logTickAndMsgType(message);
                    crashed = false;

                    // GameState.JOINED -> QUALIFYING -> RACE
                    updateGameState();

                    // using gson here because couldn't get boon to deserialize trackpiece's switchPiece boolean correctly
                    //GameInit gameInit = mapper.fromJson( line, GameInit.class );
                    GameInit gameInit = gson.fromJson( line, GameInit.class );

                    // do not init again when race starts
                    if( getCurrentGameState().equals( GameState.QUALIFYING ) ){
                        tdc.init( gameInit.getData().getRace().getTrack(), yourCar.getData(), gameInit.getData().getRace().getCars() );
                        throttlePredictor.init();
                        turboActivator.init( gameInit.getData().getRace().getRaceSession() );
//                        breakPredictor.init( 1.1, 0.1 );
                    }

                    lap = 1;

                    //if( DEBUG ) puts( "gameId: " + gameInit.getGameId() );

                    break;

                case GAME_START:
                    logTickAndMsgType(message);
                    send( new Ping(message.getGameTick()) );
                    break;

                case FULL_CAR_POSITIONS:
                case CAR_POSITIONS:
                    logTickAndMsgType(message);

                    CarPositions carPositions = mapper.fromJson( line, CarPositions.class );

                    // update car positions
                    // - updates current speed, current piece distance left and current lane
                    tdc.updateCarPositions( carPositions.getGameTick(), carPositions.getData() );

                    throttle = throttlePredictor.getEndingThrottleForTick( message.getGameTick(), lap );

                    // throttle override for brake testing while QUALIFYING
//                    if( currentGameState.equals( GameState.QUALIFYING )
//                            && (lap <= 2 && ( tdc.getOurCarPosition( message.getGameTick() ).getPiecePosition().getPieceIndex() < tdc.getTrack().getTrackPiecesAmount()/2) ) ){
                    // start breaking when speed target is reached
//                        if( breakPredictor.shouldWeBeBraking() ){
//                            throttle = 0.0;
//                            if( DEBUG ) puts( "throttle overridden for break testing! speed: " + tdc.getOurSpeed(message.getGameTick()) );
//                        }
//                    }
                    // gather break data and set new break speed target if current is reached
//                    breakPredictor.saveData( message.getGameTick(), throttle );



                    // get trackSwitchers suggestion for preferred switch direction
                    // switch lane based on best route
                    Direction preferredDirection = laneSwitcher.getPreferredSwitchDirection( message.getGameTick() );

                    if( DEBUG ) puts( "preferredDirection: " + preferredDirection );
                    if( DEBUG ) puts( "switchScheduled: " + laneSwitcher.isSwitchScheduled( message.getGameTick() ) );

                    if( crashed ) {
                        send( new Ping( message.getGameTick() ) );

                    } else if( SWITCH_LANES && tdc.getOurSpeed(message.getGameTick()) > 0 && shouldSendSwitchLaneMessage( message, preferredDirection) &&
                            !laneSwitcher.isSwitchScheduled( message.getGameTick() ) ){

                        SwitchLane switchLane = new SwitchLane( message.getGameTick(), preferredDirection );
                        send( switchLane );

                        int nextTrackPieceIndex = tdc.getOurCarPosition( message.getGameTick() ).getPiecePosition().getPieceIndex() + 1;

                        // set next switch trackpiece
                        laneSwitcher.setSwitchOnTrackPiece( tdc.getNextSwitch( nextTrackPieceIndex ) );

                    }
                    else {
                        if( USE_TURBO && turboActivator.useTurbo( message.getGameTick() ) ) {
                            if( DEBUG ) puts( "TURBO!!" );
                            send( new Turbo( message.getGameTick(), "Whiiii!!" ) );
                            turboAvailable = false;
                        } else {
                    send( new Throttle( message.getGameTick(), throttle ) );
                        }

                    }


                    CarPosition carPosition = tdc.getOurCarPosition( message.getGameTick() );
                    TrackPiecePosition trackPiecePosition = carPosition.getPiecePosition();
                    Lane lane = tdc.getTrack().getLane(trackPiecePosition.getLane().getEndLaneIndex());
                    int currentTrackPieceIndex = trackPiecePosition.getPieceIndex();
                    double pieceDistanceLeft = tdc.getTrackPieceDistanceLeft(trackPiecePosition, lane);
                    int laneIndex = lane.getIndex();
                    double angle = tdc.getOurCarPosition(message.getGameTick()).getAngle();
                    if( DEBUG ) puts(  "tick [" + message.getGameTick() + "], " + message.getMsgType()
                            + ", angle: " +  decimalFormat.format( angle )
                            + ", raw throttle: " + decimalFormat.format( throttle )
                            + ", actual throttle: " + decimalFormat.format( throttle * turboActivator.getTurboFactor() )
                            + ", speed: " + decimalFormat.format(tdc.getOurSpeed(message.getGameTick()) )
                            + ", trackpiece: " + currentTrackPieceIndex + "/" + tdc.getTrackPiecesAmount()
                            + ", pieceDistanceLeft: " + decimalFormat.format( pieceDistanceLeft )
                            + ", lane: " + laneIndex
                            + ", turboActive: " + turboActivator.isTurboActive()
//                            + ", breakTestActive: " + breakPredictor.shouldWeBeBraking()
                            + ", lap = " + lap + "/" + turboActivator.getMaxLaps()
                            + ", status = " + (crashed ? "CRASHED" : "ok"));
                    break;

                case CAR_VELOCITIES:
                    // todo: update car/track data

                    //logTickAndMsgType(message);
                    break;

                case TURBO_AVAILABLE:

                    // todo: Bump other cars off the track with turbo
                    // ignore turbo available when crashed
                    if (!crashed) {
                        TurboAvailable turboAvailableMessage = mapper.fromJson(line, TurboAvailable.class);
                        TurboAvailableData turboAvailableData = turboAvailableMessage.getData();
                        if ( turboAvailableData.getTurboDurationMilliseconds() > 0 &&
                                turboAvailableData.getTurboDurationTicks() > 0 &&
                                turboAvailableData.getTurboFactor() > 0 ) {
                            turboAvailable = true;
                        }
                        turboActivator.makeTurboAvailable( turboAvailableData );
                    }
                    break;

                case LAP_FINISHED:
                    logTickAndMsgType(message);
                    LapFinished lapFinished = mapper.fromJson( line, LapFinished.class );
                    if( lapFinished.getData().getCar().getName().equals( yourCar.getData().getName() ) ){
                        lap++;
                        turboActivator.setCurrentLap(lap);
                    }
                    break;

                case CRASH:
                    // todo: update track data
                    logTickAndMsgType(message);
                    Crash crash = mapper.fromJson(line, Crash.class);
                    if (crash.getData().equals(yourCar.getData())) {
                        ourCrashTimes = ourCrashTimes + 1;
                        crashed = true;
                        turboActivator.setTurboActive(false);
                        if ( ourCrashTimes > 1 ) {
                            //calculate new friction and maxspeeds for trackpieces
                            throttlePredictor.resetFriction();
                        }
                    }
                    break;

                case DNF:
                    logTickAndMsgType(message);
                    break;

                case SPAWN:
                    logTickAndMsgType(message);
                    Spawn spawn = mapper.fromJson(line, Spawn.class);
                    if (spawn.getData().equals(yourCar.getData())) {
                        throttle = throttlePredictor.resetCurrentThrottle();
                        send(new Throttle( message.getGameTick(), throttle ) );
                        crashed=false;
                    }
                    break;

                case LIVE_RANKING:
                    logTickAndMsgType(message);
                    break;

                case FINISH:
                    logTickAndMsgType(message);
                    Finish finish = mapper.fromJson( line, Finish.class );

                    break;

                case GAME_END:
                    logTickAndMsgType(message);
                    GameEnd gameEnd = mapper.fromJson( line, GameEnd.class );
                    puts( "race results: " );
                    for( CarResult carResult : gameEnd.getData().getResults() ){
                        puts( "car: " + carResult.getCar().getName() + ", " + carResult.getResult().getTicks() + ", " +
                                carResult.getResult().getMillis() );
                    }
                    puts( "best laps: " );
                    for( CarResult carResult : gameEnd.getData().getBestLaps() ){
                        puts( "car: " + carResult.getCar().getName() + ", " + carResult.getResult().getTicks() + ", " +
                                carResult.getResult().getMillis() );
                    }

                    //breakPredictor.printSpeedDecelerationMap();
                    break;

                case TOURNAMENT_END:
                    logTickAndMsgType(message);
                    break;

                case TURBO_START:
                    logTickAndMsgType(message);
                    TurboStart turboStart = mapper.fromJson( line, TurboStart.class);
                    if (turboStart.getData().equals(yourCar.getData())) {
                        turboActivator.setTurboActive(true);
                    }
                    break;

                case TURBO_END:
                    logTickAndMsgType(message);
                    TurboEnd turboEnd = mapper.fromJson( line, TurboEnd.class);
                    if (turboEnd.getData().equals(yourCar.getData())) {
                        turboActivator.setTurboActive(false);
                    }
                    break;

                case ERROR:
                    puts( "ERROR: " + line );
                    break;

                default:
                    logTickAndMsgType(message);
                    send( new Ping(message.getGameTick()) );
                    break;
            }

            long tickEndTime = System.currentTimeMillis();
            puts( "     [" + message.getGameTick() + "], " + message.getMsgType() + " took: " + (tickEndTime-tickStartTime) + " ms" );
            puts( " -------------------------------------" );

        }
    }

    private void updateGameState() {
        if( currentGameState.equals( GameState.JOINED ) ){
            setCurrentGameState( GameState.QUALIFYING );
        } else if( currentGameState.equals( GameState.QUALIFYING ) ){
            setCurrentGameState( GameState.RACE );
        }
        puts(  "GAMESTATE NOW: " + currentGameState );

    }

    private void logTickAndMsgType( Message message ) {
        if( DEBUG ){
            puts(  "RECEIVE [" + message.getGameTick() + "], " + message.getMsgType() );
        }
    }

    private void send( Message message ) {
        puts(  "SEND [" + message.getGameTick() + "], " + message.toString() );
        //writer.println( mapper.toJson( message ) );
        writer.println( gson.toJson( message ) );
        writer.flush();
    }

    private boolean shouldSendSwitchLaneMessage(Message message, Direction preferredDirection){

        CarPosition carPosition = tdc.getOurCarPosition( message.getGameTick() );
        TrackPiece trackPiece = tdc.getTrackPiece( carPosition.getPiecePosition().getPieceIndex() );

        TrackPiecePosition trackPiecePosition = carPosition.getPiecePosition();
        Lane lane = tdc.getTrack().getLane(trackPiecePosition.getLane().getEndLaneIndex());

        //puts(  "SWITCH [tick > 50]: " + ( message.getGameTick() > 50 ) );
        //puts(  "SWITCH [turbo not active]: " + ( !turboActivator.isTurboActive() ) );
        //puts(  "SWITCH [switchpiece is next]: " + ( tdc.getTrackPieceAhead( trackPiece, 1 ).isSwitchPiece() ) );
        //puts(  "SWITCH [current speed < " + SAFE_LANE_SWITCH_SPEED + "]: " + ( tdc.getOurSpeed(message.getGameTick()) < SAFE_LANE_SWITCH_SPEED ) );
        //puts(  "SWITCH [lane to switch exists]: " + ( laneSwitcher.canSwitchLane( preferredDirection, lane ) ) );

        return ( message.getGameTick() > 50
                && !turboActivator.isTurboActive()
                && tdc.getTrackPieceAhead( trackPiece, 1 ).isSwitchPiece()
                && tdc.getOurSpeed(message.getGameTick()) < SAFE_LANE_SWITCH_SPEED
                && !preferredDirection.equals( Direction.Straight )
                && laneSwitcher.canSwitchLane( preferredDirection, lane )
        );
    }

}

