package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.LapFinishedData;

import lombok.Data;

/**
 *
 *
 */
@Data
public class LapFinished extends Message {

    private LapFinishedData data;

    public LapFinished() {
        msgType = MessageType.LAP_FINISHED;
    }

}
