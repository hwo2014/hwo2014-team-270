package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;

import lombok.Data;

/**
 *
 * The race is about to start
 *
 */
@Data
public class GameStartsIn extends Message {

    double data;

    public GameStartsIn() {
        msgType = MessageType.GAME_STARTS_IN;
    }

}

