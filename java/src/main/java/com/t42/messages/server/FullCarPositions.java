package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.*;

import lombok.Data;

import java.util.List;

/**
 *
 * The carPositions message describes the position of each car on the track.
 *
 * The carPositions message will be sent repeatedly on each Server Tick.
 *
 */
@Data
public class FullCarPositions extends Message {

    List<FullCarPosition> data;

    public FullCarPositions() {
        msgType = MessageType.FULL_CAR_POSITIONS;
    }

}
