/**
 * 
 */
package com.t42.messages.server;

import lombok.Data;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.CarId;

/**
 * When you, or any other car uses a turbo, the server sends a turboStart message
 * 
 * @author hannuf
 *
 */
@Data
public class TurboStart extends Message {
    CarId data;

    public TurboStart() {
        msgType = MessageType.TURBO_START;
    }

}
