package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;

import lombok.Data;

/**
 *
 *
 */
@Data
public class TournamentEnd extends Message {

    private Object data;

    public TournamentEnd() {
        msgType = MessageType.TOURNAMENT_END;
    }

}
