package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.GameInitData;

import lombok.Data;

/**
 *
 * The gameInit message describes the racing track, the current racing session and the cars on track.
 *
 */
@Data
public class GameInit extends Message {

    private GameInitData data;

    public GameInit() {
        msgType = MessageType.GAME_INIT;
    }

    public GameInit( GameInitData gameInitData ) {
        super();
        data = gameInitData;
    }

}

