package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;

import lombok.Data;

/**
 *
 * The race has started!
 *
 */
@Data
public class GameStart extends Message {

    String data;

    public GameStart() {
        msgType = MessageType.GAME_START;
    }

}

