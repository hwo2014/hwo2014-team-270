package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.CarId;

import lombok.Data;

import java.util.List;

/**
 *
 *
 */
@Data
public class LiveRanking extends Message {

    List<CarId> data;

    public LiveRanking() {
        msgType = MessageType.LIVE_RANKING;
    }

}
