package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.CarId;

import lombok.Data;

/**
 *
 * This message allows the bot to identify its own car on the track. Basically it's the color that matters.
 *
 */
@Data
public class YourCar extends Message {

    private CarId data;

    public YourCar() {
        msgType = MessageType.YOUR_CAR;
    }

    public YourCar( CarId carId ) {
        super();
        data = carId;
    }
}

