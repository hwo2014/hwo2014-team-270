package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.Position;

import lombok.Data;

import java.util.List;

/**
 *
 *
 *
 */
@Data
public class CarVelocities extends Message {

    List<Position> data;

    public CarVelocities() {
        msgType = MessageType.CAR_VELOCITIES;
    }

}
