/**
 * 
 */
package com.t42.messages.server;

import lombok.Data;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.CarId;

/**
 * When the turbo expires, a turboEnd message is sent:
 * 
 * @author hannuf
 *
 */
@Data
public class TurboEnd extends Message {
    CarId data;

	public TurboEnd() {
        msgType = MessageType.TURBO_END;
    }

}
