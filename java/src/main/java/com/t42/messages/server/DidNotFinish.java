package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.DnfData;

import lombok.Data;

/**
 *
 *
 */
@Data
public class DidNotFinish extends Message {

    private DnfData data;

    public DidNotFinish() {
        msgType = MessageType.DNF;
    }

}
