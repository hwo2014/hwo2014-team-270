package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.TurboAvailableData;

import lombok.Data;

/**
 * Created by Ville on 22.4.2014.
 *
 * Turbo multiplies the power of your car's engine for a short period.
 * The server indicates the availability of turbo using the turboAvailable message.
 *
 *
 *
 */
@Data
public class TurboAvailable extends Message {

    TurboAvailableData data;
    public TurboAvailable() {
        msgType = MessageType.TURBO_AVAILABLE;
    }

}
