package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.CarId;

import lombok.Data;

/**
 *
 *
 */
@Data
public class Finish extends Message {

    private CarId data;

    public Finish() {
        msgType = MessageType.FINISH;
    }

}
