package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.GameEndData;

import lombok.Data;

/**
 *
 *
 */
@Data
public class GameEnd extends Message {

    private GameEndData data;

    public GameEnd() {
        msgType = MessageType.GAME_END;
    }

}
