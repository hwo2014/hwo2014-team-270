package com.t42.messages.server;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.CarId;

import lombok.Data;

/**
 *
 *
 *
 */
@Data
public class Crash extends Message {

    CarId data;

    public Crash() {
        msgType = MessageType.CRASH;
    }

}
