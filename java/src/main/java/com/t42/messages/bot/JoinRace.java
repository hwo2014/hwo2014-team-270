package com.t42.messages.bot;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.CreateRaceData;

import lombok.Data;

/**
 *
 * Bot sends Join, server will acknowledge this by replying with the same message.
 *
 * Maximum length for bot name is 16 characters.
 * The key is the Bot Key that you can find on your team page or in the config file in your repository.
 *
 */
@Data
public class JoinRace extends Message {

    private CreateRaceData data;

    public JoinRace() {
        msgType = MessageType.JOIN_RACE;
    }
    public JoinRace(String name, String key, String trackName, String password, int carCount) {
        msgType = MessageType.JOIN_RACE;
        data = new CreateRaceData(name, key, trackName, password, carCount);
    }

}

