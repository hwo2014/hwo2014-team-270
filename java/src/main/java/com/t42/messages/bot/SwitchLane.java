package com.t42.messages.bot;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.Direction;

import lombok.Data;

/**
 *
 *
 */
@Data
public class SwitchLane extends Message {

    private Direction data;

    public SwitchLane() {
        this.msgType = MessageType.SWITCH_LANE;
    }
    public SwitchLane( int tick, Direction direction ) {
        this.msgType = MessageType.SWITCH_LANE;
        data = direction;
        this.gameTick = tick;
    }

}
