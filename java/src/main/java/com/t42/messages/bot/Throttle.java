package com.t42.messages.bot;

import com.t42.messages.Message;
import com.t42.messages.MessageType;

import lombok.Data;

/**
 *
 * Pedal to the metal! The value 1.0 indicates full throttle.
 * Accepted values are between 0.0 and 1.0.
 *
 */
@Data
public class Throttle extends Message {

    double data;

    public Throttle() {
        msgType = MessageType.THROTTLE;
    }
    public Throttle( int tick, double throttle ) {
        msgType = MessageType.THROTTLE;
        data = throttle;
        this.gameTick = tick;
    }

}
