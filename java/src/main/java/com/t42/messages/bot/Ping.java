package com.t42.messages.bot;

import com.t42.messages.Message;
import com.t42.messages.MessageType;

public class Ping extends Message {

    public Ping(int gameTick) {
        msgType = MessageType.PING;
        this.gameTick = gameTick;
    }

}
