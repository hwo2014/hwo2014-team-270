package com.t42.messages.bot;

import com.t42.messages.Message;
import com.t42.messages.MessageType;

import lombok.Data;

/**
 * Created by Ville on 22.4.2014.
 *
 * You can use your turbo boost any time you like after the server has indicated the availability of it.
 * Once you turn it on, you can't turn it off. It'll automatically expire in 30 ticks
 * (or what ever the number was in the turboAvailable message).
 *
 */
@Data
public class Turbo extends Message {
    String data;

    public Turbo() {
        msgType = MessageType.TURBO;
    }

    public Turbo( int tick, String msg) {
        msgType = MessageType.TURBO;
        data = msg;
        gameTick = tick;
    }
}
