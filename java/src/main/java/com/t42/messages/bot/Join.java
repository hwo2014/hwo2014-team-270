package com.t42.messages.bot;

import com.t42.messages.Message;
import com.t42.messages.MessageType;
import com.t42.value.JoinData;

import lombok.Data;

/**
 *
 * Bot sends Join, server will acknowledge this by replying with the same message.
 *
 * Maximum length for bot name is 16 characters.
 * The key is the Bot Key that you can find on your team page or in the config file in your repository.
 *
 */
@Data
public class Join extends Message {

    private JoinData data;

    public Join() {
        msgType = MessageType.JOIN;
    }
    public Join( String botName, String key ) {
        msgType = MessageType.JOIN;
        data = new JoinData( botName, key );
    }

}

