package com.t42.messages;

/**
 * Server message types
 */
public class MessageType {

    public static final String JOIN = "join";
    public static final String YOUR_CAR = "yourCar";
    public static final String GAME_INIT = "gameInit";
    public static final String GAME_START = "gameStart";
    public static final String GAME_STARTS_IN = "gameStartsIn";
    public static final String CAR_POSITIONS = "carPositions";
    public static final String FULL_CAR_POSITIONS = "fullCarPositions";
    public static final String CAR_VELOCITIES = "carVelocities";
    public static final String LIVE_RANKING = "liveRanking";
    public static final String THROTTLE = "throttle";
    public static final String GAME_END = "gameEnd";
    public static final String TOURNAMENT_END = "tournamentEnd";
    public static final String CRASH = "crash";
    public static final String SPAWN = "spawn";
    public static final String DNF = "dnf";
    public static final String LAP_FINISHED = "lapFinished";
    public static final String FINISH = "finish";
    public static final String SWITCH_LANE = "switchLane";
    public static final String CREATE_RACE = "createRace";
    public static final String JOIN_RACE = "joinRace";
    public static final String PING = "ping";
    public static final String TURBO_AVAILABLE = "turboAvailable";
    public static final String TURBO = "turbo";

    public static final String ERROR = "error";
	public static final String TURBO_START = "turboStart";
	public static final String TURBO_END = "turboEnd";

/*
    JOIN( "join" ),
    YOUR_CAR( "yourCar" ),
    GAME_INIT( "gameInit" ),
    GAME_START( "gameStart" ),          // gameStart in techspec
    GAME_STARTS_IN( "gameStartsIn" ),   // gameStartsIn in test server
    CAR_POSITIONS( "carPositions" ),            // in tech spec
    FULL_CAR_POSITIONS( "fullCarPositions" ),   // in test server
    CAR_VELOCITIES( "carVelocities" ),
    LIVE_RANKING( "liveRanking" ),
    THROTTLE( "throttle" ),
    GAME_END( "gameEnd" ),
    TOURNAMENT_END( "tournamentEnd" ),
    CRASH( "crash" ),
    SPAWN( "spawn" ),
    DNF( "dnf" ),
    LAP_FINISHED( "lapFinished" ),
    FINISH( "finish" ),
    SWITCH_LANE( "switchLane" ),
    CREATE_RACE( "createRace" ),
    JOIN_RACE( "joinRace" ),
    PING( "ping" );

    private final String label;

    private MessageType(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static MessageType fromString( String label ) {
        if (label != null) {
            for( MessageType type : MessageType.values() ) {
                if( label.equalsIgnoreCase( type.label ) ) {
                    return type;
                }
            }
        }
        return null;
    }

    public String toString() {
        return this.label;
    }
*/
}
