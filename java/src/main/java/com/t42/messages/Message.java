package com.t42.messages;

import lombok.Data;

/**
 *
 * The gameTick attribute indicates the current Server Tick, which is an increasing integer.
 */
@Data
public class Message {

    public String msgType;
    public String gameId;
    public int gameTick;

}
