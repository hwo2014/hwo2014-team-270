package com.t42.value;

import lombok.Data;

/**
 * Created by Ville on 22.4.2014.
 */
@Data
public class TurboAvailableData {
    double turboDurationMilliseconds;
    int turboDurationTicks;
    double turboFactor;
}
