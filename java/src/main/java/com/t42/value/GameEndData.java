package com.t42.value;

import lombok.Data;

import java.util.List;

@Data
public class GameEndData {

    private List<CarResult> results;
    private List<CarResult> bestLaps;

}

