package com.t42.value;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import lombok.Data;

@Data
public class CarId implements Comparable<CarId> {

    private String name;
    private String color;

    public CarId(String name, String color) {
        this.name = name;
        this.color = color;
    }

	@Override
	public int compareTo(CarId o) {
		if (o == null) {
			return -1;
		}
		return ComparisonChain.start()
				.compare(this.name, o.name, Ordering.natural().nullsLast())
				.compare(this.color, o.color, Ordering.natural().nullsLast())
				.result();
	}
}
