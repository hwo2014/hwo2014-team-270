package com.t42.value;

import lombok.Data;

@Data
public class JoinData {

    private String name;
    private String key;

    public JoinData( String name, String key ) {
        this.name = name;
        this.key = key;
    }
}