package com.t42.value;

import lombok.Data;

/**
 * Created by juhak on 15/04/14.
 */
@Data
public class Lane {

    int distanceFromCenter; // A positive value tells that the lanes is to the right from the center line while
                            // a negative value indicates a lane to the left from the center.
    int index;

}
