package com.t42.value;

public enum TrackDirection {

    CLOCKWISE( "CLOCKWISE" ),
    COUNTERCLOCKWISE( "COUNTERCLOCKWISE" );

    private final String label;

    private TrackDirection(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static TrackDirection fromString( String label ) {
        if (label != null) {
            for( TrackDirection type : TrackDirection.values() ) {
                if( label.equalsIgnoreCase( type.label ) ) {
                    return type;
                }
            }
        }
        return null;
    }

    public String toString() {
        return this.label;
    }
}
