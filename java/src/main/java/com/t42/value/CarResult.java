package com.t42.value;

import lombok.Data;

@Data
public class CarResult {

    private CarId car;
    private Result result;

}
