package com.t42.value;

import lombok.Data;

import java.util.List;

/**
 * The angle depicts the car's slip angle. Normally this is zero, but when you go to a bend fast enough,
 * the car's tail will start to drift. Naturally, there are limits to how much you can drift without crashing
 * out of the track.
 */
@Data
public class CarPosition {

    CarId id;
    double angle;
    TrackPiecePosition piecePosition;

    boolean ahead;
    double distance;

    /**
     *  TODO: refactor
     *
     *  returns true is carposition is in given distance behind or ahead of the given position
     *
     * @param trackPiecePosition
     * @param distance
     * @param lastTrackPieceIndex
     * @return
     */
    public boolean isInDistanceOf( TrackPiecePosition trackPiecePosition, int distance, int lastTrackPieceIndex ) {

        int trackPieceIndex = trackPiecePosition.pieceIndex;
        int carTrackPieceIndex = piecePosition.pieceIndex;

        // min and max values
        int minTrackPieceIndex = trackPieceIndex-distance;
        int maxTrackPieceIndex = trackPieceIndex+distance;

        // if the trackPieceIndexToCompare is near track start
        if( minTrackPieceIndex < 0 ){
            minTrackPieceIndex = lastTrackPieceIndex+minTrackPieceIndex+1;

            if( ((carTrackPieceIndex >= minTrackPieceIndex) && ( carTrackPieceIndex <= lastTrackPieceIndex ))
                    || (( carTrackPieceIndex >= 0 ) && (carTrackPieceIndex <= maxTrackPieceIndex )) ){
                return true;
            }else{
                return false;
            }

        // if the trackPieceIndexToCompare is near track end
        } else if( maxTrackPieceIndex > lastTrackPieceIndex ){
            maxTrackPieceIndex = maxTrackPieceIndex-lastTrackPieceIndex-1;

            if( ((carTrackPieceIndex >= minTrackPieceIndex) && ( carTrackPieceIndex <= lastTrackPieceIndex ))
                    || (( carTrackPieceIndex >= 0 ) && (carTrackPieceIndex <= maxTrackPieceIndex )) ){
                return true;
            }else{
                return false;
            }

        }else{
            if( ((carTrackPieceIndex >= minTrackPieceIndex) && ( carTrackPieceIndex <= maxTrackPieceIndex )) ){
                return true;
            }else{
                return false;
            }
        }
    }

}
