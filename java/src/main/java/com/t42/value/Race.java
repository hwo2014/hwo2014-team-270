package com.t42.value;

import lombok.Data;

import java.util.List;

/**
 *
 *
 * The track is described as an array of pieces that may be either Straights or Bends.
 *
 * For a Straight the length attribute depicts the length of the piece. Additionally, there may be a switch attribute
 * that indicates that this piece is a switch piece, i.e. you can switch lanes here. The bridge attribute indicates
 * that this piece is on a bridge. This attribute is merely a visualization cue and does not affect the actual game.
 *
 * For a Bend, there's radius that depicts the radius of the circle that would be formed by this kind of pieces.
 * More specifically, this is the radius at the center line of the track. The angle attribute is the angle of the
 * circular segment that the piece forms. For left turns, this is a negative value. For a 45-degree bend to the left,
 * the angle would be -45. For a 30-degree turn to the right, this would be 30.
 */

@Data
public class Race {

    Track track;
    List<Car> cars;
    RaceSession raceSession;

}
