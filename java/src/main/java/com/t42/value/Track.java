package com.t42.value;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

import static org.boon.Boon.puts;

/**
 *
 * The track is described as an array of pieces that may be either Straights or Bends.
 *
 * For a Straight the length attribute depicts the length of the piece. Additionally, there may be a switch attribute
 * that indicates that this piece is a switch piece, i.e. you can switch lanes here. The bridge attribute indicates
 * that this piece is on a bridge. This attribute is merely a visualization cue and does not affect the actual game.
 *
 * For a Bend, there's radius that depicts the radius of the circle that would be formed by this kind of pieces.
 * More specifically, this is the radius at the center line of the track. The angle attribute is the angle of the
 * circular segment that the piece forms. For left turns, this is a negative value. For a 45-degree bend to the left,
 * the angle would be -45. For a 30-degree turn to the right, this would be 30.
 *
 */
@Data
public class Track {

    private String id;
    private String name;
    private List<TrackPiece> pieces;
    private List<Lane> lanes;
    private StartingPoint startingPoint;

    private NavigableMap<Integer,TrackPiece> trackPieces;
    private TrackDirection trackDirection;


    /**
     * calculates trackdirection based on trackpiece angles
     */
    public TrackDirection getTrackDirection() {

        if( trackDirection == null ){
            double totalAngle = 0.0;
            for( TrackPiece trackPiece : getPieces() ){
                totalAngle = totalAngle + trackPiece.getAngle();
            }
            trackDirection = totalAngle > 0 ? TrackDirection.CLOCKWISE : TrackDirection.COUNTERCLOCKWISE;
        }
        return trackDirection;
    }

    public TrackPiece getTrackPiece( int index ){
        return trackPieces.get( index );
    }

    public TrackPiece getLastTrackPiece(){
        return trackPieces.lastEntry().getValue();
    }


    public int getNextCurveIndex( int startIndex ){
        for( ; startIndex < trackPieces.size(); startIndex++ ){
            if( trackPieces.get( startIndex ).isCurve() ){
                return startIndex;
            }
        }
        return -1; // todo: loop... getNextCurveIndex( 0 ), tarkista ettei hajota mitaan
    }

    public int getNextSwitchIndex( int startIndex ){
        for( ; startIndex < trackPieces.size(); startIndex++ ){
            //crash! :)
            if( trackPieces.get( startIndex ).isSwitchPiece() ){
                return startIndex;
            }
        }
        return -1; // todo: loop... getNextCurveIndex( 0 ), tarkista ettei hajota mitaan
    }

    public Lane getLane( int index ){
        for( Lane lane : lanes ){
            if( lane.getIndex() == index ){
                return lane;
            }
        }
        return null;
    }

    public int getLaneAmount(){
        return lanes.size();
    }

    public int getTrackPiecesAmount() {
    	return trackPieces.size();
    }

    public Lane getRightLane( Lane currentLane ){
        int laneCount = getLaneAmount();
        if( laneCount == 1 ){
            return null;
        }

        if( trackDirection == TrackDirection.CLOCKWISE ){
            return getLane( currentLane.getIndex()+1 );
        } else{
            return getLane( currentLane.getIndex()-1 );
        }
    }

    public Lane getLeftLane( Lane currentLane ){
        int laneCount = getLaneAmount();
        if( laneCount == 1 ){
            return null;
        }

        if( trackDirection == TrackDirection.CLOCKWISE ){
            return getLane( currentLane.getIndex()-1 );
        } else{
            return getLane( currentLane.getIndex()+1 );
        }
    }

    public boolean canSwitchRight( Lane currentLane ){
        int laneCount = getLaneAmount();

        if( laneCount == 1 ){
            return false;
        }

        // A positive value tells that the lanes is to the right from the center line while
        // a negative value indicates a lane to the left from the center.
        if( currentLane.getDistanceFromCenter() > 0 ){
            // positive --> right
            if( currentLane.getIndex() == 0 ){
                return false;
            }
        }
        return true;
    }

    public boolean canSwitchLeft( Lane currentLane ){
        int laneCount = getLaneAmount();

        if( laneCount == 1 ){
            return false;
        }

        // A positive value tells that the lanes is to the right from the center line while
        // a negative value indicates a lane to the left from the center.
        if( currentLane.getDistanceFromCenter() < 0 ){
            // negative --> left
            if( currentLane.getIndex() == 0 ){
                return false;
            }
        }
        return true;
    }

}
