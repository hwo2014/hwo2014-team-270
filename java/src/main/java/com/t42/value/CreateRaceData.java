package com.t42.value;

import lombok.Data;

@Data
public class CreateRaceData {

    private BotId botId;
    private String trackName;
    private String password;
    private int carCount;

    public CreateRaceData(String name, String key, String trackName, String password, int carCount) {
    	this.botId = new BotId(name, key);
    	this.trackName = trackName;
    	this.password = password;
    	this.carCount = carCount;
    }
}
