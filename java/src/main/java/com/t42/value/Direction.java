package com.t42.value;

public enum Direction {

    Left( "Left" ),
    Right( "Right" ),
    Straight( "Straight" );

    private final String label;

    private Direction(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static Direction fromString( String label ) {
        if (label != null) {
            for( Direction type : Direction.values() ) {
                if( label.equalsIgnoreCase( type.label ) ) {
                    return type;
                }
            }
        }
        return null;
    }

    public String toString() {
        return this.label;
    }
}
