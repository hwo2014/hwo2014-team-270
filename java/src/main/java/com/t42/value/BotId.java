/**
 * 
 */
package com.t42.value;

import lombok.Data;

@Data
public class BotId {
    private String name;
    private String key;

    public BotId( String name, String key ) {
        this.name = name;
        this.key = key;
    }

}
