package com.t42.value;

import lombok.Data;

/**
 * Created by juhak on 15/04/14.
 */
@Data
public class Car {

    CarId id;
    CarDimensions dimensions;

}
