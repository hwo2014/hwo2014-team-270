package com.t42.value;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

import static org.boon.Boon.puts;

/**
 *
 * For a Straight the length attribute depicts the length of the piece. Additionally, there may be a switch attribute
 * that indicates that this piece is a switch piece, i.e. you can switch lanes here. The bridge attribute indicates
 * that this piece is on a bridge. This attribute is merely a visualization cue and does not affect the actual game.
 *
 * For a Bend, there's radius that depicts the radius of the circle that would be formed by this kind of pieces.
 * More specifically, this is the radius at the center line of the track. The angle attribute is the angle of the
 * circular segment that the piece forms. For left turns, this is a negative value. For a 45-degree bend to the
 * left, the angle would be -45. For a 30-degree turn to the right, this would be 30.
 *
 */
@Data
@ToString
public class TrackPiece {

    double length = 0.0;
    //@JsonProperty( "switch" )
    @SerializedName( "switch" )
    boolean switchPiece;
    double radius = 0.0;
    double angle = 0.0;     // left = negative angle, right = positive angle

    int index;
    Direction preferredSwitchDirection;
    double maxSpeed = 0.0;
    double maxAngle = 0.0;

    Map<Integer, Double> maxSpeedsForLanes = new HashMap<Integer, Double>();


    public TrackPieceType getTrackPieceType(){
        if( isSwitchPiece() ){
            if( angle == 0.0 && radius == 0.0 ){
                return TrackPieceType.SWITCH_STRAIGHT;
            } else {
                return TrackPieceType.SWITCH_CURVE;
            }
        }else{
            if( angle == 0.0 && radius == 0.0 ){
                return TrackPieceType.STRAIGHT;
            } else {
                return TrackPieceType.CURVE;
            }
        }
    }

    public boolean isCurve() {
        return TrackPieceType.CURVE == getTrackPieceType()|| TrackPieceType.SWITCH_CURVE == getTrackPieceType();
    }

    public boolean isStraight() {
        return TrackPieceType.STRAIGHT == getTrackPieceType() || TrackPieceType.SWITCH_STRAIGHT == getTrackPieceType();
    }

    public Direction getCurveDirection(){
        if( angle == 0.0 ){
            return Direction.Straight;
        }else if( angle < 0.0 ){
            return Direction.Left;
        }else{
            return Direction.Right;
        }
    }

    /**
     * returns current trackpiece distance left from given position
     *
     * @param inPieceDistance
     * @param lane
     * @return
     */
    public double getDistanceLeft( double inPieceDistance, Lane lane, TrackDirection trackDirection ){
        double pieceDistanceLeft = 0.0;
        if( isStraight() ) {
            pieceDistanceLeft = getLength() - inPieceDistance;
        } else if( isCurve() ) {
            //TODO is it possible to use inPieceDistance when in curve...
            pieceDistanceLeft = Math.abs( getCurveLength( lane, trackDirection )) - inPieceDistance;
        }
        return pieceDistanceLeft;
    }

    /*
     * calculate track length for curve
     */
    public double getCurveLength( Lane lane, TrackDirection trackDirection ){
        int laneDistanceFromCenter = lane.getDistanceFromCenter();

        //puts( "getCurveLength, trackDirection: " + trackDirection );
        //puts( "getCurveLength, laneDistanceFromCenter: " + laneDistanceFromCenter );

        if( trackDirection == TrackDirection.CLOCKWISE ){
            laneDistanceFromCenter = laneDistanceFromCenter * -1;
        }
        // a negative value indicates a lane to the left from the center.
        // A positive value tells that the lanes is to the right from the center line

        double laneRadius = radius + laneDistanceFromCenter;
        double length = Math.abs(angle)/360*2*Math.PI*laneRadius;

        //puts( "getCurveLength, radius: " + radius );
        //puts( "getCurveLength, laneDistanceFromCenter: " + laneDistanceFromCenter );
        //puts( "getCurveLength, angle: " + angle );
        //puts( "getCurveLength, laneRadius: " + laneRadius );
        //puts( "getCurveLength, length: " + length );
        return length;
    }

    public boolean isPieceSimilar( TrackPiece trackPiece ) {
        return this.getTrackPieceType() == trackPiece.getTrackPieceType() &&
                ((this.angle == trackPiece.getAngle() && this.radius == trackPiece.getRadius()) ||
                        this.length == trackPiece.getLength());
    }

    public double getMaxSpeedForLane(int laneIndex) {
        return maxSpeedsForLanes.get(laneIndex);
    }

    public void setMaxSpeedForLane(int laneIndex, double maxSpeed) {
        maxSpeedsForLanes.put(laneIndex, maxSpeed);
    }

    public double getLengthForStraightOrCurve(Lane lane, TrackDirection trackDirection) {
        if ( isStraight() ) {
            return getLength();
        } else {
            return getCurveLength(lane, trackDirection);
        }
    }
}
