package com.t42.value;

/**
 * Created by Ville on 27.4.2014.
 */
public enum PieceChangePartType {
    STRAIGHT_TO_CURVE,
    CURVE_TO_STRAIGHT,
    CURVE_TO_CURVE,
}
