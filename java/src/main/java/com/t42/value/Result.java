package com.t42.value;

import lombok.Data;

@Data
public class Result {

    private int lap;
    private int laps;
    private int ticks;
    private int millis;
    private String reason;  // from test server data

}

