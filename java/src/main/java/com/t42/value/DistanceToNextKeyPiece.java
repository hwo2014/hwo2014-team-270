package com.t42.value;

import lombok.Data;

/**
 * Key piece is totally different than current piece (from piece)
 *
 * Created by Ville on 25.4.2014.
 */
@Data
public class DistanceToNextKeyPiece {
    private TrackPiece fromPiece;
    private TrackPiece toPiece;
    private double distance;

    private DistanceToNextKeyPiece keyPiece2;
    private DistanceToNextKeyPiece keyPiece3;

    public PieceChangePartType getChangePartType() {
        if ( fromPiece != null && toPiece != null ) {
            if ( fromPiece.isStraight() && toPiece.isCurve() ) {
                return PieceChangePartType.STRAIGHT_TO_CURVE;
            } else if ( fromPiece.isCurve() && toPiece.isStraight() ) {
                return PieceChangePartType.CURVE_TO_STRAIGHT;
            } else if ( fromPiece.isCurve() && toPiece.isCurve()) {
                return PieceChangePartType.CURVE_TO_CURVE;
            }
        }
        return null;
    }
}
