package com.t42.value;

import lombok.Data;

/**
 *
 * The piecePosition object consists of
 *
 * pieceIndex - zero based index of the piece the car is on
 * inPieceDistance - the distance the car's Guide Flag (see above) has traveled from the start of the piece along the
 *                  current lane
 * lane - a pair of lane indices. Usually startLaneIndex and endLaneIndex are equal, but they do differ when the car
 *      is currently switching lane
 * lap - the number of laps the car has completed. The number 0 indicates that the car is on its first lap.
 * The number -1 indicates that it has not yet crossed the start line to begin it's first lap.
 *
 */
@Data
public class TrackPiecePosition {

    int pieceIndex;
    double inPieceDistance;
    LaneIndex lane;
    int lap;

}
