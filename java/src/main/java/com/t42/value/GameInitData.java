package com.t42.value;

import lombok.Data;

@Data
public class GameInitData {

    private Race race;

}
