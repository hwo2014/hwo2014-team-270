package com.t42.value;

public enum TrackPieceType {

    STRAIGHT( "STRAIGHT" ),
    CURVE( "CURVE" ),
    SWITCH_STRAIGHT( "SWITCH_STRAIGHT" ),
    SWITCH_CURVE( "SWITCH_CURVE" );

    private final String label;

    private TrackPieceType(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static TrackPieceType fromString( String label ) {
        if (label != null) {
            for( TrackPieceType type : TrackPieceType.values() ) {
                if( label.equalsIgnoreCase( type.label ) ) {
                    return type;
                }
            }
        }
        return null;
    }

    public String toString() {
        return this.label;
    }
}
