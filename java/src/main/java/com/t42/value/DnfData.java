package com.t42.value;

import lombok.Data;

import java.util.List;

@Data
public class DnfData {

    private CarId car;
    private String reason;

}
