package com.t42.value;

import lombok.Data;

import java.util.List;

@Data
public class LapFinishedData {

    private CarId car;

    private Result lapTime;
    private Result raceTime;
    private Ranking ranking;

}

