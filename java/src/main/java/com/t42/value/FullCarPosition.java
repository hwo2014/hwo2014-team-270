package com.t42.value;

import lombok.Data;

/**
 * The angle depicts the car's slip angle. Normally this is zero, but when you go to a bend fast enough,
 * the car's tail will start to drift. Naturally, there are limits to how much you can drift without crashing
 * out of the track.
 */
@Data
public class FullCarPosition {

    CarId id;
    Position coordinates;
    double angle;
    double angleOffset;
    TrackPiecePosition piecePosition;

}
